/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <at86rf215.h>
#include <etl/vector.h>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/msgq.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <satnogs-comms/rf_frontend09.hpp>
#include <satnogs-comms/rf_frontend24.hpp>
#include <satnogs-comms/temperature.hpp>
#include <satnogs-comms/version.hpp>
#include <type_traits>

namespace satnogs::comms
{

/**
 * @brief Exception indicating a generic exception of the \ref radio subsystem
 * @note This exception has exception::severity::MAJOR severity
 * @ingroup exceptions
 */
class radio_exception : public exception
{
public:
  radio_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MAJOR, "Radio error",
                            "radioerr", ERADIO})
  {
  }
};

/**
 * @brief Exception indicating an invalid frequency
 * @note This exception has exception::severity::MAJOR severity
 * @ingroup exceptions
 */
class unsupported_freq_exception : public exception
{
public:
  unsupported_freq_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MAJOR,
                            "Unsupported frequency error", "freqerr", EBADFREQ})
  {
  }
};

/*
 * This is a weak function of the AT86RF215 driver allowing user defined
 * processing of the IRQs. We need to forward declare it here in order
 * to define it as friend of the radio class.
 * With this trick, this callback can access the private fields of the radio
 * class. This eliminate the need for the radio class to expose at the public
 * API, dangerous/unnecessary methods
 */
extern "C" int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs);

/**
 * @ingroup rffe
 * @{
 * @brief Radio subsystem providing TX/RX functionality on UHF and S-Band
 *
 * The \ref radio class is the main control interface for both UHF and S-Band
 * interfaces as well as the RF frontends.
 *
 * TX of frames is implemented for now using blocking mode. The calling task
 * that calls the radio::tx() blocks until the successful transmission of the
 * desired frame, or until a timeout. This is expected to change in later
 * releases of the library.
 *
 * On the other hand, reception is performed asynchronously. After a call of the
 * radio::rx_async(), the radio enters RX mode and the calling task continues
 * its execution. The radio will continue to stay in RX mode until either the
 * radio interface is disabled, or a TX is performed by calling the radio::tx().
 *
 */
class radio
{
public:
  /**
   * AGC Vout for the UHF is at channel 2 of the DAC
   *
   */
  static const uint16_t UHF_AGC_VOUT_DAC_CH = 2;

  /**
   * AGC Vout for the UHF is at channel 2 of the DAC
   *
   */
  static const uint16_t SBAND_AGC_VOUT_DAC_CH = 1;

  /**
   * @brief Configuration parameters for the RF mixer
   *
   */
  class mixer_param
  {
  public:
    std::pair<float, float> freq; //!< The frequency range that this
                                  //!< configuration parameter is valid
    float lo; //!< The LO frequency that should be used for this particular
              //!< frequency range
  };

  class frame_len
  {
  public:
    uint32_t tx;
    uint32_t rx;
  };

  /**
   * @brief Radio interface identifier
   *
   */
  enum class interface : uint8_t
  {
    UHF   = 0, //!< UHF
    SBAND = 1  //!< S-BAND
  };

  /**
   * @brief Radio Operational mode
   */
  enum class op_mode : uint8_t
  {
    BASEBAND, //!< The baseband core is active
    IQ        //!< The IQ functionality is active
  };

  /**
   * @brief IO configuration that is necessary for the radio to operate
   *
   */
  class io_conf
  {
  public:
    bsp::spi    &spi_ctrl; //!< SPI device controlling the AT86RF215
    bsp::gpio   &nreset;   //!< nRST signal for the AT86RF215
    bsp::chrono &chrono;   //!< A chrono device, necessary for any wait or time
                           //!< measurements required by the radio
    rf_frontend09::io_conf
        &rffe09_io; //!< The IO configuration of the UHF frontend. @see
                    //!< rf_frontend09::io_conf for more details
    rf_frontend24::io_conf
        &rffe24_io; //!< The IO configuration of the S-Band frontend. @see
                    //!< rf_frontend24::io_conf for more details
  };

  /**
   * @brief Initialization parameters of the radio class
   *
   * @details This class encapsulates all the necessary parameters
   * required for the initialization of the radio subsystem
   *
   * @note SatNOGS-COMMS operates using fixed-sized frames. The frame
   * length for the UHF and S-Band should be set during the initialization of
   * the radio subsystem
   *
   * @note The S-Band interface uses an RF mixer to properly tune to the desired
   * frequency. The user can/is required to specify a set of frequency ranges
   * and a desired LO frequency for the mixer for both TX and RX. Currently the
   * number of different ranges that are supported is 2 per direction.
   */
  class params
  {
  public:
    rf_frontend::params rffe09_params; //!< Parameters of the UHF frontend
    rf_frontend::params rffe24_params; //!< Parameters of the S-Band frontend
    frame_len           uhf_len;       //!< The frame length of the UHF radio
    frame_len           sband_len;     //!< The frame length of the UHF radio
    std::array<mixer_param, 2>
        tx_mixer_params; //!< RF mixer parameters for the TX path
    std::array<mixer_param, 2>
        rx_mixer_params; //!< RF mixer parameters for the RX path
  };

  /**
   * @brief FSK configuration parameters
   *
   */
  class fsk_conf
  {
  public:
    at86rf215_fsk_srate_t rate; //!< FSK data rate
    float mod_idx; //!< FSK modulation index. Use 0.5 for MSK. The radio
                   //!< supports quantized modulation index values. The closest
                   //!< to the given on wiil be applied
    float excess_bw; //!< FSK 3-dB bandwidth of the shaping filter
  };

  /**
   * @brief Metadata for a received frame
   *
   */
  class rx_info
  {
  public:
    float rssi; //!< RSSI of the frame in dB. Can be NaN if the RSSI cannot be
                //!< calculated
    float     edv;   //!< Not currently in use
    uint32_t  len;   //!< The frame length
    interface iface; //!< Interface from which the frame was received
  };

  /**
   * @brief The RX message accompanied by its metadata
   *
   * @note The maximum frame size is bounded by the FIFO of the AT86RF215 and it
   * is 2047 bytes
   */
  class rx_msg
  {
  public:
    uint8_t pdu[AT86RF215_MAX_PDU]; //!< Buffer to hold the received frame.
    rx_info info;                   //!< Metadata
  };

  /**
   * @brief RX configuration parameters
   */
  class rx_conf
  {
  public:
    float                       freq; //!< The desired RX frequency to set
    fsk_conf                    fsk;  //!< FSK parameters
    rf_frontend::rx_gain_params gain; //!< Gain settings to set
  };

  /**
   * @brief Radio interface statistics
   *
   */
  class stats
  {
  public:
    size_t tx_frames;       //!< TX frames successfully sent
    size_t tx_frames_fail;  //!< Number of TX frames that failed to be sent due
                            //!< to an error
    size_t tx_frames_drop;  //!< TX frames dropped due to full message queue
    size_t rx_frames;       //!< RX frames successfully received and passed any
                            //!< validity checks
    size_t rx_frames_inval; //!< Number of RX frames that failed to pass the
                            //!< validity checks
    size_t rx_frames_drop;  //!< RX frames dropped due to a full message queue

    stats()
        : tx_frames(0),
          tx_frames_fail(0),
          tx_frames_drop(0),
          rx_frames(0),
          rx_frames_inval(0),
          rx_frames_drop(0)
    {
    }
  };

  radio(const params &p, const io_conf &cnf, power &pwr,
        bsp::imsgq<rx_msg> &rx_msgq);

  static void
  trx_irq_handler();

  void
  enable(bool yes = true);

  void
  enable(interface iface, bool yes = true);

  bool
  enabled() const;

  bool
  enabled(interface iface) const;

  rf_frontend::dir
  direction(interface iface) const;

  void
  set_frequency(interface iface, rf_frontend::dir d, float freq);

  float
  frequency(interface iface, rf_frontend::dir d) const;

  void
  set_tx_gain(interface iface, float gain);

  void
  set_test_fsk(interface iface);

  void
  tx(interface iface, const uint8_t *b, size_t len);

  void
  rx_async(interface iface, const rx_conf &conf);

  bool
  mixer_lock();

  rf_frontend09 &
  uhf();

  rf_frontend24 &
  sband();

  int
  recv_msg(rx_msg &msg, size_t timeout_ms);

  const stats &
  get_stats(interface iface) const;

  uint32_t
  tx_len(interface iface) const;

  uint32_t
  rx_len(interface iface) const;

  void
  reset_stats(interface iface);

private:
  /* IQIFSF TRXERR BATLOW EDC TRXRDY*/
  static constexpr uint8_t TX_RF_IRQM = 0xFE;
  /* All IRQs for the baseband core enabled during TX */
  static constexpr uint8_t TX_BB_IRQM = 0xFF;

  /* IQIFSF TRXERR BATLOW EDC TRXRDY*/
  static constexpr uint8_t RX_RF_IRQM = 0xFE;

  /* RXFE, RXFS */
  static constexpr uint8_t RX_BB_IRQM = 0x3;

  const frame_len            m_frame_len[2];
  bsp::spi                  &m_spi;
  bsp::gpio                 &m_nrst;
  std::array<mixer_param, 2> m_tx_mixer_params;
  std::array<mixer_param, 2> m_rx_mixer_params;
  mutable struct at86rf215   m_at86;
  bool                       m_iface_enabled[2];
  rf_frontend09              m_rffe09;
  rf_frontend24              m_rffe24;
  bsp::chrono               &m_chrono;
  power                     &m_pwr;
  bsp::imsgq<rx_msg>        &m_rxq;
  stats                      m_stats[2];

  void
  bsp_setup();

  void
  at86rf215_enable(bool enable = true);

  friend int
  at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                              uint8_t rf24_irqs, uint8_t bbc0_irqs,
                              uint8_t bbc1_irqs);

  void
  incr_tx_frames(interface iface);

  void
  incr_tx_fail_frames(interface iface);

  void
  incr_tx_drop_frames(interface iface);

  void
  incr_rx_frames(interface iface);

  void
  incr_rx_inval_frames(interface iface);

  void
  incr_rx_drop_frames(interface iface);

  uint32_t
  iface_idx(interface iface) const;

  void
  set_rx_gain(interface iface, const rf_frontend::rx_gain_params &gain);

  float
  lo_freq(float rf_freq, rf_frontend::dir d) const;
  float
  if_freq(float rf_freq, rf_frontend::dir d) const;
};

/** @} */

} // namespace satnogs::comms