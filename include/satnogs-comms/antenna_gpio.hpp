/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <initializer_list>
#include <satnogs-comms/antenna.hpp>
#include <satnogs-comms/bsp/gpio.hpp>

namespace satnogs::comms
{

/**
 * @ingroup antenna
 * @{
 * @brief This class implements a simple GPIO-based antenna deployment
 * mechanism, using for each controllable element a GPIO for instructing the
 * deployment and a GPIO for sensing the deployment status of the corresponding
 * element.
 *
 * @tparam N the number of elements
 *
 * @note The template parameter N can be omitted, as it can be deduced
 * automatically by the constructor \ref antenna_gpio().
 */
template <uint32_t N> class antenna_gpio : public antenna
{
public:
  /**
   * @brief Specifies the deploy and the sensing GPIO
   *
   */
  class elem_io
  {
  public:
    bsp::gpio &deploy;
    bsp::gpio &sense;
  };

  /**
   * @brief Construct a new antenna_gpio object.
   *
   * @param name the name of the antenna
   * @param io a \ref elem_io for each controllable element of the antenna
   */
  antenna_gpio(const etl::istring &name, const elem_io (&&io)[N])
      : antenna(name, N), m_elems(io)
  {
    static_assert(N > 0, "At least one IO configuration should be set");
  }

  bool
  detect(uint32_t idx) override
  {
    if (idx > N - 1) {
      throw inval_arg_exception(__FILE__, __LINE__);
    }
    return m_elems[idx].sense.get();
  }

  void
  deploy(uint32_t idx, bool en)
  {
    if (idx > N - 1) {
      throw inval_arg_exception(__FILE__, __LINE__);
    }
    m_elems[idx].deploy.set(en);
  }

private:
  const elem_io m_elems[N];
};

/** @} */
} // namespace satnogs::comms
