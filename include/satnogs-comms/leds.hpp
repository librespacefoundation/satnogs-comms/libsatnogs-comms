/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <etl/flat_map.h>
#include <satnogs-comms/bsp/gpio.hpp>
namespace satnogs::comms
{
/**
 * @brief LED control library for the SatNOGS-COMMS board
 *
 * This class allows controlling the LEDs on the SatNOGS-COMMS board using
 * basic operations such as toggling, enabling, disabling, and checking their
 * state.
 *
 * @ingroup bsp
 */
class leds
{
public:
  /**
   * @brief Enumerates the available LEDs on the SatNOGS-COMMS board
   */
  enum class led : uint8_t
  {
    led0, /**< Represents LED_0 */
    led1  /**< Represents LED_1 */
  };

  leds(bsp::gpio &led0, bsp::gpio &led1);

  void
  toggle(led x);

  void
  enable(led x, bool en = true);

  void
  disable(led x);

  bool
  enabled(led x) const;

private:
  etl::flat_map<led, bsp::gpio &, 2> m_leds;
};

} // namespace satnogs::comms