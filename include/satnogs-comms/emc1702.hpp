/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <etl/string.h>
#include <etl/vector.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{
/**
 * @brief Interface for the EMC1702 High-Side Current-Sense and Dual
 * Temperature Monitor
 *
 * The `emc1702` class provides methods to configure and retrieve data from the
 * EMC1702 I2C sensor, enabling precise monitoring and control of current,
 * voltage, and temperature.
 *
 * Features:
 * -# **Temperature Monitoring**:
 *   - Monitoring of the temperature of one externally connected diode.
 *   - Monitoring of the internal or ambient temperature
 *   - Configurable temperature limits for high, low, and critical thresholds.
 *
 * -# **Voltage Monitoring**:
 *   - High-side voltage sensing with configurable limits.
 *   - Critical voltage thresholds with hysteresis control.
 *
 * -# **Current Monitoring**:
 *   - High-side current sensing using an external resistor.
 *   - Bi-directional current measurement with configurable sampling and
 * averaging.
 *
 * -# **Peak Detection**:
 *   - Independent circuitry for current peak detection with programmable
 * duration and thresholds.
 *
 * For detailed specifications, refer to the EMC1702 Datasheet:
 * https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/DataSheets/EMC1702-Data-Sheet-DS20006455A.pdf
 *
 */
class emc1702
{
public:
  /**
   * @brief Full Scale Voltage in volts.
   */
  static constexpr float fsv = 23.9883;

  /**
   * @brief External sense resistor value in Ohms.
   */
  static constexpr float r_sense = 0.01;

  /**
   * @brief Status register flags
   */
  struct status
  {
    bool busy        = 0; /**< Sensor is busy */
    bool crit        = 0; /**< Critical condition detected */
    bool diode_fault = 0; /**< External diode fault detected */
    bool low         = 0; /**< Low voltage/current condition detected */
    bool high        = 0; /**< High voltage/current condition detected */
    bool peak        = 0; /**< Peak detection triggered */
  };

  /**
   * @brief Selects the mode of operation of the EMC1702 sensor
   */
  enum class sensor_mode : uint8_t
  {
    FULLY_ACTIVE     = 0b00100000, /**< Fully active mode */
    CURRENT_ONLY     = 0b01100000, /**< Current measurement only */
    TEMPERATURE_ONLY = 0b00100100, /**< Temperature measurement only */
    STANDBY          = 0b01100100  /**< Standby mode */
  };

  /**
   * @brief Settings to set the frequency of measurements in the Conversion Rate
   * Register
   *
   * The Conversion Rate Register controls how often the source voltage and
   * temperature measurement channels are updated and compared against the
   * limits.
   */
  enum class conversion_rate : uint8_t
  {
    PER_1_SEC_8_MEAS  = 0b00000111, /**< 8 measurements per second */
    PER_1_SEC_4_MEAS  = 0b00000110, /**< 4 measurements per second */
    PER_1_SEC_2_MEAS  = 0b00000101, /**< 2 measurements per second */
    PER_1_SEC_1_MEAS  = 0b00000100, /**< 1 measurement per second */
    PER_2_SEC_1_MEAS  = 0b00000011, /**< 1 measurement every 2 seconds */
    PER_4_SEC_1_MEAS  = 0b00000010, /**< 1 measurement every 4 seconds */
    PER_8_SEC_1_MEAS  = 0b00000001, /**< 1 measurement every 8 seconds */
    PER_16_SEC_1_MEAS = 0b00000000  /**< 1 measurement every 16 seconds */
  };

  /**
   * @brief Settings to set the threshold for consecutive diode fault alerts in
   * the Consecutive Alert Register
   *
   * The Consecutive Alert Register determines how many times an out-of-limit
   * error or diode fault must be detected in consecutive measurements before
   * the interrupt status registers are asserted. This applies to temperature
   * limits only.
   */
  enum class consecutive_alert_diode_fault : uint8_t
  {
    OUT_OF_LIM_MEAS_1 =
        0b00000000, /**< Trigger after 1 out-of-limit measurement */
    OUT_OF_LIM_MEAS_2 =
        0b00000010, /**< Trigger after 2 out-of-limit measurements */
    OUT_OF_LIM_MEAS_3 =
        0b00000110, /**< Trigger after 3 out-of-limit measurements */
    OUT_OF_LIM_MEAS_4 =
        0b00001110 /**< Trigger after 4 out-of-limit measurements */
  };

  /**
   * @brief Settings to set the threshold for consecutive voltage out-of-limit
   * measurements in the Voltage Sampling Configuration Register
   */
  enum class consecutive_alert_voltage : uint8_t
  {
    OUT_OF_LIM_MEAS_1 =
        0b00000000, /**< Trigger after 1 out-of-limit measurement */
    OUT_OF_LIM_MEAS_2 =
        0b10000100, /**< Trigger after 2 out-of-limit measurements */
    OUT_OF_LIM_MEAS_3 =
        0b10001000, /**< Trigger after 3 out-of-limit measurements */
    OUT_OF_LIM_MEAS_4 =
        0b10001100 /**< Trigger after 4 out-of-limit measurements */
  };

  /**
   * @brief Settings to set the threshold for consecutive current out-of-limit
   * measurements in the Current Sense Sampling Configuration Register
   */
  enum class consecutive_alert_current : uint8_t
  {
    OUT_OF_LIM_MEAS_1 =
        0b00000000, /**< Trigger after 1 out-of-limit measurement */
    OUT_OF_LIM_MEAS_2 =
        0b01000000, /**< Trigger after 2 out-of-limit measurements */
    OUT_OF_LIM_MEAS_3 =
        0b10000000, /**< Trigger after 3 out-of-limit measurements */
    OUT_OF_LIM_MEAS_4 =
        0b11000000 /**< Trigger after 4 out-of-limit measurements */
  };

  /**
   * @brief Settings to set the Beta Compensation factor in the Beta
   * Configuration Registers
   *
   * The Beta Compensation factor is used for the external diode channels to
   * optimize temperature measurements. This enum provides predefined settings
   * to configure the Beta Compensation factor directly or enable autodetection
   * circuitry for automatic optimization.
   *
   * @note Care should be taken to avoid setting a Beta value higher than the
   * transistor's beta when autodetection is disabled, as it may introduce
   * measurement errors.
   *
   * @note When measuring a discrete thermal diode (such as 2N3904) or a
   * CPU diode that functions like a discrete thermal diode
   * (such as an AMD processor diode), the Beta Compensation should be set to
   * DISABLED
   */
  enum class beta_config : uint8_t
  {
    MIN_BETA_0_050 = 0b00000000, /**< Beta Compensation factor = 0.050 */
    MIN_BETA_0_066 = 0b00000001, /**< Beta Compensation factor = 0.066 */
    MIN_BETA_0_087 = 0b00000010, /**< Beta Compensation factor = 0.087 */
    MIN_BETA_0_114 = 0b00000011, /**< Beta Compensation factor = 0.114 */
    MIN_BETA_0_150 = 0b00000100, /**< Beta Compensation factor = 0.150 */
    MIN_BETA_0_197 = 0b00000101, /**< Beta Compensation factor = 0.197 */
    MIN_BETA_0_260 = 0b00000110, /**< Beta Compensation factor = 0.260 */
    MIN_BETA_0_342 = 0b00000111, /**< Beta Compensation factor = 0.342 */
    MIN_BETA_0_449 = 0b00001000, /**< Beta Compensation factor = 0.449 */
    MIN_BETA_0_591 = 0b00001001, /**< Beta Compensation factor = 0.591 */
    MIN_BETA_0_778 = 0b00001010, /**< Beta Compensation factor = 0.778 */
    MIN_BETA_1_024 = 0b00001011, /**< Beta Compensation factor = 1.024 */
    MIN_BETA_1_348 = 0b00001100, /**< Beta Compensation factor = 1.348 */
    MIN_BETA_1_773 = 0b00001101, /**< Beta Compensation factor = 1.773 */
    MIN_BETA_2_333 = 0b00001110, /**< Beta Compensation factor = 2.333 */
    DISABLED = 0b00001111, /**< Disable Beta Compensation for discrete thermal
                              diodes. */
    AUTO = 0b00010000 /**< Enable automatic Beta Compensation autodetection. */
  };

  /**
   * @brief Settings to set the Ideality Factor in the External Diode
   * Ideality Factor Registers
   *
   * The Ideality Factor Registers store the ideality factors that are applied
   * to the external diodes and influence temperature measurements.
   *
   * @note Beta Compensation and Resistance Error Correction automatically
   * correct for most diode ideality errors; therefore, it is not
   * recommended that these settings be updated without consulting Microchip.
   *
   * @note Recommended Settings:
   * Intel CPUs (65nm): Use the default setting `IDEALITY_FACTOR_1_0080` (12h).
   * Intel CPUs (45nm): Use `IDEALITY_FACTOR_1_0119` (15h).
   */
  enum class ideality_factor : uint8_t
  {
    IDEALITY_FACTOR_1_0053 = 0b00010000, /**< Ideality factor = 1.0053 (10h) */
    IDEALITY_FACTOR_1_0066 = 0b00010001, /**< Ideality factor = 1.0066 (11h) */
    IDEALITY_FACTOR_1_0080 = 0b00010010, /**< Ideality factor = 1.0080 (12h),
                                            default for 65nm Intel CPUs */
    IDEALITY_FACTOR_1_0093 = 0b00010011, /**< Ideality factor = 1.0093 (13h) */
    IDEALITY_FACTOR_1_0106 = 0b00010100, /**< Ideality factor = 1.0106 (14h) */
    IDEALITY_FACTOR_1_0119 = 0b00010101, /**< Ideality factor = 1.0119 (15h),
                                            recommended for 45nm Intel CPUs */
    IDEALITY_FACTOR_1_0133 = 0b00010110, /**< Ideality factor = 1.0133 (16h) */
    IDEALITY_FACTOR_1_0146 = 0b00010111  /**< Ideality factor = 1.0146 (17h) */
  };

  /**
   * @brief Settings to set the digital averaging on mesurements
   * in the Averaging Control Register, the Voltage Sampling Configuration
   * Register, or the Current Sense Sampling Configuration Register
   *
   * The averaging control determines how many measurements are averaged
   * to produce a single temperature, source voltage, or current reading.
   */
  enum class averaging_control : uint8_t
  {
    DISABLED = 0b00000000, /**< Averaging is disabled; raw readings are used. */
    AVERAGING2X = 0b00000001, /**< 2x averaging */
    AVERAGING4X = 0b00000010, /**< 4x averaging */
    AVERAGING8X = 0b00000011  /**< 8x averaging */
  };

  /**
   * @brief Settings to set the Current Sensing Sampling Time
   * in the Current Sense Sampling Configuration Register
   *
   * The current sensing sampling time determines the integration period over
   * which the current is averaged. The sampled VSENSE voltage reflects the
   * average current over this period multiplied by the selected averaging
   * factor.
   *
   * @note Longer sampling times improve resolution, but reduce responce time.
   */
  enum class current_sampling_time : uint8_t
  {
    S_T_82_MS  = 0b00000000, /**< Sampling time = 82 ms (default) */
    S_T_164_MS = 0b00001000, /**< Sampling time = 164 ms */
    S_T_328_MS = 0b00001100  /**< Sampling time = 328 ms */
  };

  /**
   * @brief Settings to set the Current Sense maximum expected voltage (full
   * scale range) in the Current Sense Sampling Configuration Register
   */
  enum class max_expected_voltage : uint8_t
  {
    CURRENT_SENSOR_RANGE_10_mV = 0b00000000, /**< Maximum voltage = 10 mV */
    CURRENT_SENSOR_RANGE_20_mV = 0b00000001, /**< Maximum voltage = 20 mV */
    CURRENT_SENSOR_RANGE_40_mV = 0b00000010, /**< Maximum voltage = 40 mV */
    CURRENT_SENSOR_RANGE_80_mV =
        0b00000011 /**< Maximum voltage = 80 mV (default) */
  };

  /**
   * @brief Settings to set the peak detector threshold level in the Peak
   * Detection Configuration Register
   */
  enum class peak_detection_threshold : uint8_t
  {
    THRESHOLD_10_mV = 0b00000000, /**< Threshold = 10 mV */
    THRESHOLD_15_mV = 0b00010000, /**< Threshold = 15 mV */
    THRESHOLD_20_mV = 0b00100000, /**< Threshold = 20 mV */
    THRESHOLD_25_mV = 0b00110000, /**< Threshold = 25 mV */
    THRESHOLD_30_mV = 0b01000000, /**< Threshold = 30 mV */
    THRESHOLD_35_mV = 0b01010000, /**< Threshold = 35 mV */
    THRESHOLD_40_mV = 0b01100000, /**< Threshold = 40 mV */
    THRESHOLD_45_mV = 0b01110000, /**< Threshold = 45 mV */
    THRESHOLD_50_mV = 0b10000000, /**< Threshold = 50 mV */
    THRESHOLD_55_mV = 0b10010000, /**< Threshold = 55 mV */
    THRESHOLD_60_mV = 0b10100000, /**< Threshold = 60 mV */
    THRESHOLD_65_mV = 0b10110000, /**< Threshold = 65 mV */
    THRESHOLD_70_mV = 0b11000000, /**< Threshold = 70 mV */
    THRESHOLD_75_mV = 0b11010000, /**< Threshold = 75 mV */
    THRESHOLD_80_mV = 0b11100000, /**< Threshold = 80 mV */
    THRESHOLD_85_mV = 0b11110000  /**< Threshold = 85 mV */
  };

  /**
   * @brief Settings to set the peak detector minimum time threshold in the Peak
   * Detection Configuration Register
   */
  enum class peak_detection_duration : uint8_t
  {
    DURATION_1_00_ms    = 0b00000000, /**< Duration = 1.00 ms */
    DURATION_5_12_ms    = 0b00000001, /**< Duration = 5.12 ms */
    DURATION_25_60_ms   = 0b00000010, /**< Duration = 25.60 ms */
    DURATION_51_20_ms   = 0b00000011, /**< Duration = 51.20 ms */
    DURATION_76_80_ms   = 0b00000100, /**< Duration = 76.80 ms */
    DURATION_102_40_ms  = 0b00000101, /**< Duration = 102.40 ms */
    DURATION_128_00_ms  = 0b00000110, /**< Duration = 128.00 ms */
    DURATION_256_00_ms  = 0b00000111, /**< Duration = 256.00 ms */
    DURATION_384_00_ms  = 0b00001000, /**< Duration = 384.00 ms */
    DURATION_512_00_ms  = 0b00001001, /**< Duration = 512.00 ms */
    DURATION_768_00_ms  = 0b00001010, /**< Duration = 768.00 ms */
    DURATION_1024_00_ms = 0b00001011, /**< Duration = 1024.00 ms */
    DURATION_1536_00_ms = 0b00001100, /**< Duration = 1536.00 ms */
    DURATION_2048_00_ms = 0b00001101, /**< Duration = 2048.00 ms */
    DURATION_3072_00_ms = 0b00001110, /**< Duration = 3072.00 ms */
    DURATION_4096_00_ms = 0b00001111  /**< Duration = 4096.00 ms */
  };

  emc1702(const etl::istring &name, bsp::i2c &i2c, uint16_t addr,
          bsp::gpio &alert);

  emc1702(const etl::istring &name, bsp::i2c &i2c, uint16_t addr);

  void
  set_peak_detection_config(peak_detection_threshold t,
                            peak_detection_duration  d);

  void
  get_measurements(sensor_mode m);

  float
  get_temperature_internal() const;

  float
  get_temperature_external() const;

  void
  get_status(status &s);

  void
  set_config(sensor_mode m) const;

  void
  set_conversion_rate(conversion_rate r) const;

  void
  set_int_temp_high_lim(float int_temp_high_lim = 85.0f) const; // in °C

  void
  set_int_temp_low_lim(float int_temp_low_lim = -128.0f) const; // in °C

  void
  set_ext_temp_high_lim(float ext_temp_high_lim = 85.0f) const; // in °C

  void
  set_ext_temp_low_lim(float ext_temp_low_lim = -128.0f) const; // in °C

  void
  get_one_shot_meas() const;

  void
  set_t_crit(float ext_t_crit = 100.0f, float int_t_crit = 100.0f,
             float t_crit_hysterisis = 10.0f) const; // in °C

  bool
  get_diode_fault() const;

  void
  set_consecutive_alert(consecutive_alert_diode_fault c,
                        consecutive_alert_diode_fault l) const;

  void
  set_beta_configuration(beta_config b) const;

  void
  set_ideality_factor(ideality_factor i) const;

  etl::vector<bool, 4>
  get_high_limit_status() const;

  etl::vector<bool, 4>
  get_low_limit_status() const;

  bool
  get_crit_limit_status() const;

  void
  set_averaging_control(averaging_control a) const;

  bool
  get_sensor_info() const;

  void
  set_voltage_sampling_config(consecutive_alert_voltage v,
                              averaging_control         s) const;

  void
  set_current_sense_sampling_config(consecutive_alert_current v,
                                    averaging_control         i,
                                    current_sampling_time     t,
                                    max_expected_voltage      m) const;

  void
  set_peak_detection_config(peak_detection_threshold t,
                            peak_detection_duration  d) const;

  float
  get_sense_current() const;

  float
  get_source_voltage() const;

  float
  get_power() const;

  void
  set_v_sense_lim(float v_sense_lim_low  = 2032.0f,
                  float v_sense_lim_high = -2033.0f) const;

  void
  set_v_source_lim(float v_source_lim_low  = 0.0f,
                   float v_source_lim_high = 23.9063f) const; // in V

  void
  set_v_crit_lim(float v_sense_lim_crit    = 2032.0f,        // in mV
                 float v_source_lim_crit   = 23.9063f,       // in V
                 float v_sense_hysterisis  = 160.0f,         // in mV
                 float v_source_hysterisis = 0.9375f) const; // in V

  bool
  alert() const;

  float
  get_temperature_average() const; // in °C

  float
  get_fsc() const;

private:
  etl::string<32>               m_name;
  bsp::i2c                     &m_i2c;
  const uint32_t                m_addr;
  bsp::gpio                    &m_alert;
  sensor_mode                   mode;
  conversion_rate               rate;
  averaging_control             avrg_temp;
  consecutive_alert_diode_fault alert_crit;
  consecutive_alert_diode_fault alert_lim;
  beta_config                   beta;
  ideality_factor               i_factor;
  consecutive_alert_voltage     v_source_spike;
  averaging_control             avrg_v_source;
  consecutive_alert_current     v_sense_spike;
  averaging_control             avrg_i_sense;
  current_sampling_time         i_sampling;
  max_expected_voltage          v_sense_max;
  peak_detection_threshold      thres;
  peak_detection_duration       dur;
  max_expected_voltage          range;
  status                        s;
  bsp::dummy_gpio               m_dummy;
};

/**
 * @brief Exception thrown when a thermal shutdown is required.
 *
 * This exception is triggered when the temperature exceeds the critical
 * threshold, necessitating a thermal shutdown to prevent damage.
 *
 * @ingroup exceptions
 */
class emc1702_thermal_shutdown_needed : public exception
{
public:
  emc1702_thermal_shutdown_needed(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MINOR,
                            "Thermal Shutdon is required (from emc1702)",
                            "thermalshutdownemc1702", ETHERMSHUT})
  {
  }
};

/**
 * @brief Exception thrown when sensor information retrieval fails, indicating
 * errors in I2C communication.
 *
 * This exception indicates an error in communication with the EMC1702 sensor,
 * potentially due to hardware or connection issues.
 *
 * @ingroup exceptions
 */
class emc1702_incorrect_sensor_info : public exception
{
public:
  emc1702_incorrect_sensor_info(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MINOR,
                            "Error in getting sensor info. Probably there "
                            "is an error in communication",
                            "incorrectsensinfo", EBADINFSENS})
  {
  }
};

/**
 * @brief Exception thrown when the EMC1702 is busy.
 *
 * This exception is triggered when a read or measurement request is made while
 * the EMC1702's ADCs are still converting.
 *
 * @ingroup exceptions
 */
class emc1702_not_ready : public exception
{
public:
  emc1702_not_ready(string_type file_name, numeric_type line)
      : exception(
            file_name, line,
            error_msg{exception::severity::MINOR,
                      "The ADCs of emc1702 currently converting. Readings not "
                      "ready",
                      "emc1702_busy", EBUSYSENS})
  {
  }
};

} // namespace satnogs::comms