/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <at86rf215.h>
#include <etl/string.h>
#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/dac.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/f2972.hpp>
#include <satnogs-comms/power.hpp>
#include <utility>

namespace satnogs::comms
{

/**
 *
 *
 */
class rf_frontend
{
public:
  /**
   * @brief RF frontend initialization settings
   *
   */
  class params
  {
  public:
    const std::pair<float, float> rx_range; //!< RX tuning range
    const std::pair<float, float> tx_range; //!< TX tuning range
    const std::pair<float, float>
        agc0_range; //!< The signal level range that the first stage AGC can
                    //!< achieve
    const std::pair<float, float>
        gain0_range; //!< The gainl range of the first gain stage
    const std::pair<float, float>
        agc0_calib; //!< First stage AGC linear transformation settings (slope,
                    //!< intercept point)
    //!< Those should be acquired after calibration procedure and normally
    //!< stored to a persistent storage in the firmware
    const std::pair<float, float>
        gain0_calib; //!< First stage gain linear transformation settings
                     //!< (slope, intercept point)
    //!< Those should be acquired after calibration procedure and normally
    //!< stored to a persistent storage in the firmware
  };

  class io_conf
  {
  public:
    bsp::gpio &en_agc;
    bsp::dac  &agc_vset;
    bsp::gpio &flt_sel;
  };

  /**
   * Direction / interface
   */
  enum class dir : uint8_t
  {
    RX, /**< RX interface */
    TX, /**< TX interface */
    INVALID
  };

  /**
   * RX Gain mode
   */
  enum class gain_mode : uint8_t
  {
    AUTO,  /**< AGC enabled */
    MANUAL /**< AGC bypassed */
  };

  enum class filter : uint8_t
  {
    WIDE,
    NARROW
  };

  /**
   * @brief RX gain settings for the two different gain stages.
   * Gain0 stage corresponds to the first stage (closest to the antenna),
   * implemented by the AD8318. The second stage is implemented by the
   * AT86RF215.
   *
   */
  class rx_gain_params
  {
  public:
    gain_mode gain0_mode; /**< Gain mode of the first gain stage */
    union
    {
      float tgt;  //!< Target signal level for the AGC [-60, -35]
      float gain; //!< Fixed gain value in dB [-6, 29.5]
    } gain0;
    gain_mode gain1_mode; /**< Gain mode of the second gain stage */
    union
    {
      at86rf215_agc_conf agc; //!< AGC configuration for the AT86RF215
                              //!< Fields at86rf215_agc_conf.enable and
                              //!< at86rf215_agc_conf.freeze are explicitly set
                              //!< to 1 and 0 respectively
      float gain;             //! Fixed gain value in dB [0, 69]
    } gain1;
  };

  rf_frontend(const params &init_params, io_conf &&io, power &pwr);

  /**
   * Enable/Disable the RF frontend and its associated components
   * @param set true to enable, false to disable
   */
  virtual void
  enable(bool set = true) = 0;

  virtual bool
  enabled() const = 0;

  virtual void
  set_filter(filter f);

  virtual filter
  get_filter() const;

  virtual void
  set_direction(dir d, float lo_freq = 0.0f) = 0;

  dir
  direction() const;

  void
  set_rx_gain(const rx_gain_params &gain);

  bool
  frequency_valid(dir d, float freq) const;

protected:
  const params m_params;
  bsp::gpio   &m_filt_sel;
  bsp::dac    &m_dac;
  power       &m_pwr;
  ad8318       m_agc;
  dir          m_dir;
};

} // namespace satnogs::comms