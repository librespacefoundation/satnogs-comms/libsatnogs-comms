/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <cstdlib>

namespace satnogs::comms::bsp
{
/**
 * @brief Message queue device abstraction
 *
 * This template class provides a generic message queue abstraction for messages
 * of type @ref T.
 *
 * @warning Depending on the target platform/RTOS users are expected to define a
 * class that inherits this one and implement at least the pure virtual methods
 *
 * @tparam T The type of the messages stored in the queue
 * @ingroup bsp
 */
template <typename T> class imsgq
{
public:
  /**
   * @brief Constructs an `imsgq` object with a specified maximum size.
   *
   * @param len The maximum number of messages the queue can hold.
   */
  imsgq(size_t len) : m_len(len) {}

  imsgq(const imsgq &) = delete;

  /**
   * @brief A method that returns the maximum size of the message queue.
   *
   * @return The maximum number of messages the queue can hold.
   */
  size_t
  max_size() const
  {
    return m_len;
  }

  /**
   * @brief A virtual method that returns the current number of messages in the
   * queue.
   *
   * @return The number of messages currently in the queue.
   *
   * @note Must be implemented by the inherited class.
   */
  virtual size_t
  size() = 0;

  /**
   * @brief A virtual method that enqueues a message into the queue.
   *
   * @param msg The message of type @ref T to enqueue.
   * @param timeout_ms The timeout in milliseconds to wait if the queue is full.
   * @return success or error code
   *
   * @note Must be implemented by the inherited class.
   */
  virtual int
  put(const T &msg, size_t timeout_ms) = 0;

  /**
   * @brief A virtual method that enqueues a message into the queue from an
   * interrupt context.
   *
   * @param msg The message of type @ref T to enqueue.
   * @return success or error code
   * @note Must be implemented by the inherited class.
   */
  virtual int
  put_isr(const T &msg) = 0;

  /**
   * @brief A virtual method that peeks at the next message in the queue without
   * removing it.
   *
   * @param msg Pointer to store the peeked message of type @ref T.
   * @return success or error code
   * @note Must be implemented by the inherited class.
   */
  virtual int
  peek(T *msg) = 0;

  /**
   * @brief A virtual method that retrieves a message from the queue.
   *
   * @param msg Pointer to store the retrieved message of type @ref T.
   * @param timeout_ms The timeout in milliseconds to wait if the queue is
   * empty.
   * @return success or error code
   * @note Must be implemented by the inherited class.
   */
  virtual int
  get(T *msg, size_t timeout_ms) = 0;

  /**
   * @brief A virtual method that retrieves a message from the queue from an
   interrupt context.
   *
   * @param msg Pointer to store the retrieved message.
   * @return success or error code
   * @note Must be implemented by the inherited class.
   */
  virtual int
  get_isr(T *msg) = 0;

protected:
  const size_t m_len;
};

/**
 * @brief Message queue device abstraction with custom maximum number of
 * messages
 *
 * This template class provides a generic message queue abstraction with
 * capacity @ref LEN for messages of type @ref T. This class inherits from @ref
 * imsgq.
 *
 * @warning Depending on the target platform/RTOS users are expected to define a
 * class that inherits this one and implement at least the pure virtual methods
 *
 * @tparam T The type of the messages stored in the queue
 * @tparam LEN The maximum number of messages the queue can hold.
 * @ingroup bsp
 */
template <typename T, const size_t LEN> class msgq : public imsgq<T>
{
public:
  msgq() : imsgq<T>(LEN) {}
};
} // namespace satnogs::comms::bsp
