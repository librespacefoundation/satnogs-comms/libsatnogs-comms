/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <etl/algorithm.h>

namespace satnogs::comms::bsp
{
/**
 * @brief DAC device abstraction
 *
 * This class provides a generic DAC (Digital-to-Analog Converter) abstraction.
 *
 * @warning Depending on the target platform/RTOS users are expected to define a
 * class that inherits this one and implement at least the pure virtual methods
 *
 * @ingroup bsp
 */
class dac
{
public:
  /**
   * @brief Construct a new DAC object
   *
   * @param resolution The resolution of the DAC in number of bits
   * @param vref The reference voltage for the DAC in Volts
   */
  dac(uint16_t resolution, float vref)
      : m_res(resolution), m_vref(vref), m_volts(0.0f)
  {
  }

  /**
   * @brief A virtual method to start the DAC.
   *
   * Prepares the DAC for operation.
   *
   * @note This method must be implemented by the inherited class to configure
   * the DAC and must be invoked before operation.
   */
  virtual void
  start() = 0;

  /**
   * @brief A virtual method to stop the DAC.
   *
   * This method must be provided by the child class.
   */
  virtual void
  stop() = 0;

  /**
   * @brief Retrieves the latest output voltage of the DAC.
   *
   * @return The latest output voltage, in Volts.
   */
  virtual float
  voltage() const
  {
    return m_volts;
  }

  /**
   * @brief Sets the output voltage of the DAC.
   *
   * This method calculates the digital value corresponding to the desired
   * output voltage. The voltage is clamped between 0V and the reference
   * voltage.
   *
   * @param volts The desired output voltage, in Volts.
   */
  virtual void
  set_voltage(float volts)
  {
    m_volts    = etl::clamp(volts, 0.0f, m_vref);
    uint32_t x = m_volts / (vref() / (1UL << m_res));
    write(x);
  }

  /**
   * @brief Retrieves the reference voltage of the DAC.
   *
   * @return The reference voltage, in Volts.
   */
  virtual float
  vref() const
  {
    return m_vref;
  }

protected:
  virtual void
  write(uint32_t x) = 0;

private:
  const uint16_t m_res;
  const float    m_vref;
  uint32_t       m_volts;
};

} // namespace satnogs::comms::bsp
