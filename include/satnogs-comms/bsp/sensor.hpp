/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>

namespace satnogs::comms::bsp
{
/**
 * @brief Sensor device abstraction
 *
 * This class provides a generic abstraction for accessing internal ADC channel
 * sensor readings of the SoC(e.g.STM32H743XI), such as internal VBAT (battery
 * voltage) and VREF (reference voltage).
 *
 * @warning Depending on the target platform/RTOS users are expected to define a
 * class that inherits this one and implement at least the pure virtual methods
 *
 * @ingroup bsp
 */
class sensor
{
public:
  /**
   * @brief Construct a new sensor object
   *
   */
  sensor() {}
  ~sensor() {}

  /**
   * @brief Get the VBAT from SoCs designated internal ADC channel
   *
   * @return float VBAT voltage in Volts
   */
  virtual float
  vbat() = 0;

  /**
   * @brief Get the VREF from SoCs designated internal ADC channel
   *
   * @return float VBAT voltage in Volts
   */
  virtual float
  vref() = 0;
};

} // namespace satnogs::comms::bsp
