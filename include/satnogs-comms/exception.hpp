/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <etl/error_handler.h>
#include <satnogs-comms/errno_extended.hpp>
namespace satnogs::comms
{

/**
 *
 * @ingroup exceptions
 * @{
 * @brief Exception base class
 *
 * Each exception of SatNOGS-COMMS control library has a \ref severity level
 * that allows the firmware to take actions based on the severity. In addition
 * the class provides the filename as well as the code line from which the
 * exception was thrown. Also two different messages are supported. A verbose
 * one that can be used during development and terse,, concise one that can be
 * used for logging during in-flight.
 *
 * Each exception is categorized by a severity level, enabling the firmware to
 * take appropriate actions based on the severity of the issue. <b>Key Features
 * of the exceptions system:</b>
 * - Severity Levels: Differentiates exceptions based on their impact on system
 * operation.
 * - Filename and Line Number: Provides precise information about the origin of
 * the exception.
 * - Verbose and Terse Messages: Offers detailed explanations for development
 * purposes and concise logs for in-flight operations.
 *
 * @note Exception handling is a task of the firmware/user
 *
 */
class exception : public etl::exception
{
public:
  /**
   * @brief Severity levels of exceptions
   *
   * @see FDIR analysis at https://cloud.libre.space/s/xzskpy8m3Nb54YL
   */
  enum class severity : uint8_t
  {
    CATASTROPHIC = 0, /**< Failure causing loss of mission */
    CRITICAL = 1, /**< Failure causing major mission degradation or significant
                     damage */
    MAJOR = 2,    /**< Failure causing minor mission degradation */
    MINOR = 3,    /**< Failure causing minimal impact */
    NONE  = 4     /**< No failure */
  };

  /**
   * @brief A class representing error messages in the SatNOGS-COMMS system.
   */
  class error_msg
  {
  public:
    exception::severity
        svrt; /**< The severity level of the error as defined in FDIR. */
    const char *verbose; /**< A verbose description of the error. Will be
                            used if `ETL_VERBOSE_ERRORS` is defined. */
    const char
        *terse;         /**< A terse, concise description of the error.
                           Will be used if `ETL_VERBOSE_ERRORS` is not defined. */
    uint32_t error_num; /**< A numeric error code representing the error type.
                           Can be found either in `errno.hpp` or
                           `errno_extended.hpp`. */
  };

  /**
   * @brief Constructor for the exception class.
   *
   * @param file The file where the exception was thrown.
   * @param lineno The line number where the exception was thrown.
   * @param err_msg The error message object containing the error details.
   */
  exception(const char *file, int lineno, error_msg err_msg)
      : etl::exception(ETL_ERROR_TEXT(err_msg.verbose, err_msg.terse), file,
                       lineno),
        m_err_msg(err_msg)
  {
  }

  uint32_t
  get_errno() const
  {
    return m_err_msg.error_num;
  }
  /**
   * @brief Get the verbose error message
   *
   * Base class returns either verbose or terse message using the what() method
   * based on the definition of ETL_VERBOSE_ERRORS
   * configuration. This method provides the user with the ability to access the
   * verbose error message in any case.
   *
   * @return string_type the verbose error message
   */
  string_type
  what_verbose() const
  {
    return m_err_msg.verbose;
  }

  /**
   * @brief Get the terse error message
   *
   * Base class returns either verbose or terse message using the what() method
   * based on the definition of ETL_VERBOSE_ERRORS configuration. This method
   * provides the user with the ability to access the verbose error message in
   * any case.
   *
   * @return string_type the terse error message
   */
  string_type
  what_terse() const
  {
    return m_err_msg.terse;
  }

  /**
   * @brief Get error severity level as defined in FDIR.
   *
   * The severity levels are defined in exception::severity enum class.
   *
   * @return exception::severity the severity level of the error.
   */
  severity
  get_severity() const
  {
    return m_err_msg.svrt;
  }

  const error_msg
  get_error_msg() const
  {
    return m_err_msg;
  }

private:
  const error_msg m_err_msg;
};

/**
 * @brief Generic exception indicating an invalid argument
 * @note This exception has exception::severity::MINOR severity
 * @ingroup exceptions
 */
class inval_arg_exception : public exception
{
public:
  inval_arg_exception(const char *file_name, int line)
      : exception(file_name, line,
                  error_msg{exception::severity::MINOR, "Invalid argument",
                            "invalarg", EINVAL})
  {
  }
};

/**
 * @brief Generic exception indicating that a message requested for
 * processing/handling was larger than the expected.
 * @note This exception has exception::severity::MINOR severity
 * @ingroup exceptions
 */
class msg_too_long_exception : public exception
{
public:
  msg_too_long_exception(const char *file_name, int line)
      : exception(file_name, line,
                  error_msg{exception::severity::MINOR, "Message too long",
                            "msgbadsize", EMSGSIZE})
  {
  }
};

/**
 * @brief Generic exception indicating an a resource or subsystem is not
 * available at the time that was requested
 * @note This exception has exception::severity::MINOR severity
 * @ingroup exceptions
 */
class resource_unavailable_exception : public exception
{
public:
  resource_unavailable_exception(const char *file_name, int line)
      : exception(
            file_name, line,
            error_msg{exception::severity::MINOR,
                      "The resource you are trying to access is currently "
                      "unavailable",
                      "eagain", EAGAIN})
  {
  }
};

/**@} */

} /* namespace satnogs::comms */