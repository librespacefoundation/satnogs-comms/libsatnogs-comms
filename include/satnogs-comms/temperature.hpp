/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/emc1702.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/version.hpp>

namespace satnogs::comms
{
/**
 * @brief Source of temperature readings.
 */
enum class temperature_sensor : uint8_t
{
  PCB,     //!< PCB temperature sensor
  UHF_PA,  //!< UHF PA temperature sensor
  SBAND_PA //!< S-Band PA temperature sensor
};

/**
 * @brief Exception class for accessing an invalid temperature sensor
 *
 * Thrown when an operation attempts to access or configure an unsupported
 * or invalid temperature sensor.
 *
 * @note This exception has exception::severity::MAJOR severity
 * @ingroup exceptions
 */
class invalid_sensor_exception : public exception
{
public:
  invalid_sensor_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MAJOR, "Invalid sensor error",
                            "invalsenserr", EINVALSENS})

  {
  }
};
/**
 * @brief Manages multiple temperature sensors for monitoring and alerting.
 *
 * The `temperature` class interfaces with multiple temperature sensors
 * to monitor temperatures for various components, including PCB, UHF PA, and
 * S-Band PA. It provides methods for retrieving temperatures and handling
 * alerts.
 *
 * @tparam T The type of temperature sensor (For the time being only `emc1702`
 * is supported).
 *
 * @ingroup temperature
 */
template <typename T> class temperature
{
public:
  temperature(bsp::i2c &i2c, bsp::gpio &alert_t_pa_uhf,
              bsp::gpio &alert_t_pa_sband, const rf_frontend09 &rf09,
              const rf_frontend24 &rf24)
      : m_pcb(etl::make_string("pcb"), i2c, pcb_addr),
        m_uhf(etl::make_string("pa-uhf"), i2c, uhf_addr, alert_t_pa_uhf),
        m_sband(etl::make_string("pa-sband"), i2c, sband_addr,
                alert_t_pa_sband),
        m_rf09(rf09),
        m_rf24(rf24)

  {
    static_assert(std::is_same_v<emc1702, T>, "Unsupported temperature sensor");
  }

  // Contructor specialiazation if needed can be applied by using:
  //
  // template <typename X = T, typename std::enable_if_t<std::is_same_v<
  //                               temperature<X>, temperature<emc1702>>>>
  // temperature(bsp::i2c &i2c)
  // {
  // }

  /**
   * @brief Retrieve the average board temperature
   *
   * @return float The average temperature parsed from the three available
   * temperature sensors (PCB, UHF PA, SBAND PA) on the Satnogs Comms board
   */
  float
  get() const
  {
    return (get(temperature_sensor::PCB) + get(temperature_sensor::UHF_PA) +
            get(temperature_sensor::SBAND_PA)) /
           3.0f;
  }

  /**
   * @brief Returns the temperature from a specific sensor
   *
   * @param s the target sensor
   * @return float The temperature from a specific sensor
   * @throws invalid_sensor_exception if the specified sensor is invalid.
   */
  float
  get(temperature_sensor s) const;

  /**
   * @brief Checks if any temperature sensor has triggered an alert.
   *
   * @return `true` if any sensor has an active alert, `false` otherwise.
   */
  bool
  alert() const
  {
    return alert(temperature_sensor::PCB) ||
           alert(temperature_sensor::UHF_PA) ||
           alert(temperature_sensor::SBAND_PA);
  }

  /**
   * @brief Checks if a specific temperature sensor has triggered an alert.
   *
   * @param s The sensor to check for an alert condition.
   * @return `true` if the specified sensor has an active alert, `false`
   * otherwise.
   *
   * @throws invalid_sensor_exception if the specified sensor is invalid.
   */
  bool
  alert(temperature_sensor s) const;

private:
  static constexpr uint8_t pcb_addr = 0b101101;
  static constexpr uint8_t uhf_addr = 0b1001101;
  static constexpr uint8_t sband_addr =
      version::num(0, 3, 1) < version::hw() ? 0b101001 : 0b11000;

  T                    m_pcb;
  T                    m_uhf;
  T                    m_sband;
  const rf_frontend09 &m_rf09;
  const rf_frontend24 &m_rf24;
};

} // namespace satnogs::comms
