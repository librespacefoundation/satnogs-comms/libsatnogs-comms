/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024-2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/radio.hpp>

namespace satnogs::comms
{

/**
 * @ingroup antenna
 * @{
 * @brief Generic antenna definition
 *
 * The goal of this class is to provide a common generic interface that will
 * simplify implementations of antenna deployment. The class is not intended to
 * be used directly, rather than be inherited by classes implementing the actual
 * interaction with the hardware (e.g GPIO, SPI, I2C, etc)
 *
 */
class antenna
{
public:
  antenna(const etl::istring &name, uint32_t nelems);

  virtual bool
  detect(uint32_t idx);

  virtual void
  deploy(uint32_t idx, bool en);

  const char *
  name() const;

  uint32_t
  nelems() const;

protected:
  const etl::istring &m_name;
  const uint32_t      m_nelems;
};

/** @} */

} // namespace satnogs::comms
