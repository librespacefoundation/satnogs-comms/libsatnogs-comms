/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <type_traits>

#include <satnogs-comms/antenna_gpio.hpp>
#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <satnogs-comms/emc1702.hpp>
#include <satnogs-comms/emmc.hpp>
#include <satnogs-comms/fpga.hpp>
#include <satnogs-comms/leds.hpp>
#include <satnogs-comms/power.hpp>
#include <satnogs-comms/radio.hpp>
#include <satnogs-comms/temperature.hpp>
#include <satnogs-comms/version.hpp>

namespace satnogs::comms
{
/**
 * @{
 * @ingroup libsatnogs-comms
 * @defgroup board Board interface
 *
 * @brief A singleton class for the entire board management
 *
 * This class provides a singleton interface for managing and controlling the
 * entire board. By adhering to the singleton design pattern, it ensures that
 * the firmware maintains a single instance of the board, simplifying usage and
 * preventing potential race conditions and duplication.
 *
 * This class offers a singleton for the entire board management and control.
 * Following this design pattern, it is assured that the entire firmware has
 * only one instance declared for the board. This simplifies the usage of the
 * control library and avoid any duplication and race conditions.
 *
 * @warning The singleton pattern inherently limits the ability to pass
 * parameters to the class instance. To address this, the init() static
 * method is provided for initialization. This method creates the unique
 * instance and throws an initialization_exception if called multiple times.
 * Once initialized, the get_instance() method grants access to the
 * single \ref board instance. Calling get_instance() before initialization will
 * result in an uninitialization_exception.
 *
 * The initialized \ref board instance provides access to various subsystems and
 * peripherals, as illustrated in the schematic below.
 *
 * @mermaid{board}
 */
class board
{
public:
  /**
   * @brief IO peripherals required for the control of the various subsystems of
   * the board
   *
   * @ingroup board
   */
  class io_conf
  {
  public:
    bsp::gpio &emmc_en;     //!< GPIO for the eMMC enable
    bsp::gpio &emmc_sel;    //!< GPIO for the eMMC direction selections
    bsp::gpio &emmc_rst;    //!< GPIO for the reset signal of the eMMC
    bsp::spi  &fpga_spi;    //!< SPI peripheral for communication with the FPGA
    bsp::gpio &led0;        //!< GPIO of the LED0
    bsp::gpio &led1;        //!< GPIO of the LED1
    power::io_conf &pwr_io; //!< IO configuration of the power subsystem
    bsp::i2c
        &sensors_i2c; //!< i2c peripheral for communication with the available
                      //!< sensors (temperature, current, voltage)
    bsp::gpio &alert_t_pa_uhf;   //!< Temperature alert GPIO for the UHF PA
    bsp::gpio &alert_t_pa_sband; //!< Temperature alert GPIO for the S-Band PA
    radio::io_conf &radio_io;    //!< IO configuration of the radio subsystem
    bsp::chrono
        &chrono; //!< Chrono device for measuring time and introducing delay
    bsp::gpio
        &fpga_done; //!< GPIO indicating successful firmware load on the FPGA
    comms::antenna &uhf_antenna;   //!< UHF antenna interface
    comms::antenna &sband_antenna; //!< S-Band antenna interface
  };

  /**
   * @brief Board initialization parameters
   *
   * @ingroup board
   */
  class params
  {
  public:
    radio::params radio_params; //!< radio subsystem initialization parameters
    fpga::hw      fpga_hw;      //!< FPGA type currently attached on the board
    bsp::imsgq<radio::rx_msg>
        &rx_msgq; //!< Thread-safe message queue for holding received frames
                  //!< from the radio interfaces
  };

  static void
  init(const io_conf &io, const params &p);

  /* Singleton */
  static board &
  get_instance();

  static bool
  is_init();

  board(board const &) = delete;

  void
  operator=(board const &) = delete;

  comms::radio &
  radio();

  comms::emmc &
  emmc();

  comms::power &
  power();

  comms::fpga &
  fpga();

  comms::leds &
  leds();

  comms::antenna &
  antenna(radio::interface iface);

  bool
  alert(temperature_sensor s) const;

  float
  temperature(temperature_sensor s) const;

protected:
  board(const io_conf &io, const params &p);

private:
  bool m_init;
  /*
   * power subsystem should be first, because many of the other subsystems
   * depends on it
   */
  comms::power                m_power;
  comms::radio                m_radio;
  comms::fpga                 m_fpga;
  comms::emmc                 m_emmc;
  comms::antenna              m_uhf_antenna;
  comms::antenna              m_sband_antenna;
  comms::leds                 m_leds;
  comms::temperature<emc1702> m_temperature;
};

/** @} */

/**
 * @brief Raised if the get_instance() is called before the init()
 *
 * @ingroup exceptions
 */
class uninitialization_exception : public exception
{
public:
  uninitialization_exception(string_type file_name, numeric_type line)
      : exception(
            file_name, line,
            error_msg{exception::severity::MAJOR,
                      "Singleton class not initialized (init() not called)",
                      "noinit", EUNINIT})
  {
  }
};

/**
 * @brief Raised if the init() is called more than once
 *
 * @ingroup exceptions
 */
class initialization_exception : public exception
{
public:
  initialization_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::CRITICAL,
                            "Singleton class already initialized", "init",
                            EINIT})
  {
  }
};

} // namespace satnogs::comms
