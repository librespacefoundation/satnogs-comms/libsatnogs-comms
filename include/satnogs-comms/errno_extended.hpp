/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include <errno.h>

/**
 * @file errno_extended.hpp
 * @brief Defines custom error codes for the SatNOGS-COMMS control library.
 *
 * This file introduces a set of error codes specific to the SatNOGS-COMMS
 * control library, extending the standard error codes provided by `<errno.h>`.
 *
 * @note The error codes start from `__ELASTERROR + 1` to ensure they do not
 * conflict with standard error codes.
 */

extern "C" {
#define EINVALSENS (__ELASTERROR + 1) //!< Invalid sensor
#define EUNINIT                                                                \
  (__ELASTERROR + 2) //!< Singleton not initialized (init() not called)
#define EINIT        (__ELASTERROR + 3) //!< Singleton already initialized
#define ETHERMSHUT   (__ELASTERROR + 4) //!< Thermal shutdown is required
#define EBADINFSENS  (__ELASTERROR + 5) //!< Parsing bad sensor info
#define EBUSYSENS    (__ELASTERROR + 6) //!< Readings from sensor not available yet
#define EHWNOTCONF   (__ELASTERROR + 7)  //!< Pin is not configured
#define ERADIO       (__ELASTERROR + 8)  //!< Error in radio
#define EBADFREQ     (__ELASTERROR + 9)  //!< Unsupported frequency
#define ERFFE24      (__ELASTERROR + 10) //!< Error in rf_frontend24
#define EMIXLOCK     (__ELASTERROR + 11) //!< Error in mixer lock
#define EI2C         (__ELASTERROR + 12) //!< Error in I2C
#define ESPI         (__ELASTERROR + 13) //!< Error in SPI
#define ECANENBL     (__ELASTERROR + 14) //!< Error in enabling CAN device
#define EISOTPBIND   (__ELASTERROR + 15) //!< Error in ISOTP device binding
#define EISOTPRECV   (__ELASTERROR + 16) //!< Error in receiving from ISOTP device
#define EDEVNOTREADY (__ELASTERROR + 17) //!< Device not ready
#define EVBAT        (__ELASTERROR + 18) //!< Error in VBAT readings
#define EADCINIT     (__ELASTERROR + 19) //!< Error in ADC readings
#define EDEVNOTCONF  (__ELASTERROR + 20) //!< Device not configured
#define EWDT         (__ELASTERROR + 21) //!< Watchdog error
}
