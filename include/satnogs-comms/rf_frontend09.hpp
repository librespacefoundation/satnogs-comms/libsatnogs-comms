/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <satnogs-comms/lna.hpp>
#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

/**
 * @ingroup rffe
 * @{
 * @brief RF-frontend for the UHF interface
 *
 * This class provides methods to fully control the UHF frontend.
 *
 * The control of the direction for this particular frontend is fully automatic
 * utilizing the AT86RF215 capabilities. Users can control the reception filter
 * (wideband vs narrowband) as well as the first stage variable gain amplifier
 * (VGA) of the RX chain. This VGA can operate either in fixed gain mode, either
 * as AGC.
 *
 * The general architecture of the frontend is summarized in the schematic
 * below.
 *
 * \image html uhf-radio.png
 * \image latex uhf-radio.png "UHF RF frontend architecture"
 *
 */
class rf_frontend09 : public rf_frontend
{
public:
  rf_frontend09(const params &init_params, io_conf &io, power &pwr);

  void
  enable(bool set = true);

  bool
  enabled() const;

  void
  set_direction(dir d, float lo_freq = 0);
};

/** @} */
} /* namespace satnogs::comms */