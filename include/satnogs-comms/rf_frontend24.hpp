/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <rffcx07x.h>
#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/f2972.hpp>
#include <satnogs-comms/lna.hpp>
#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

/**
 * @brief Exception for the S-Band RF-frontend
 *
 * @note This exception has exception::severity::MAJOR severity
 * @ingroup exceptions
 */
class rf_frontend24_exception : public exception
{
public:
  rf_frontend24_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MAJOR,
                            "rf_frontend24 exception", "rffe24err", ERFFE24})
  {
  }
};

/**
 * @brief Exception occurred when the RF mixer fails to lock
 *
 * @note This exception has exception::severity::MAJOR severity
 * @ingroup exceptions
 */
class mixer_lock_exception : public exception
{
public:
  mixer_lock_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{exception::severity::MAJOR, "mixer lock exception",
                            "mixlockerr", EMIXLOCK})
  {
  }
};

/**
 * @ingroup rffe
 * @{
 * @brief RF-frontend for the S-Band interface
 *
 * This class provides methods to fully control the S-Band frontend.
 *
 * The frontend of the S-Band RF frontend share similarities with the
 * rf_frontend09, with the added complexity of an RF Mixer which is responsible
 * to properly tune the radio to the desired frequency ranges that are not
 * covered by the AT86RF215 frequency synthesizer. The control of the direction
 * for this particular frontend is fully automatic utilizing the AT86RF215
 * capabilities. Users can control the reception filter (wideband vs narrowband)
 * as well as the first stage variable gain amplifier (VGA) of the RX chain.
 * This VGA can operate either in fixed gain mode, either as AGC.
 *
 * The general architecture of the frontend is summarized in the schematic
 * below.
 *
 * \image html s-band-radio.png
 * \image latex s-band-radio.png "S-Band RF frontend architecture"
 */
class rf_frontend24 : public rf_frontend
{
public:
  /**
   * @brief IO configuration for controlling the various peripherals of the
   * S-Band frontend
   *
   */
  class io_conf
  {
  public:
    bsp::gpio &en_agc;    //!< The AGC enable GPIO
    bsp::dac  &agc_vset;  //!< DAC to control the VGA
    bsp::gpio &flt_sel;   //!< Filter selection GPIO
    bsp::gpio &mixer_clk; //!< GPIO for the mixer clock
    bsp::gpio &mixer_rst; //!< GPIO for the mixer reset signal
    bsp::gpio &mixer_enx; //!< GPIO for the mixer enable signal (active low)
    bsp::gpio &mixer_sda; //!< GPIO for the mixer data transfer (bidirectional)
    bsp::chrono &chrono;  //!< Chrono device for fetching time information and
                          //!< for introducing delay
  };

  rf_frontend24(const params &init_params, const io_conf &io, power &pwr);

  void
  enable(bool set = true);

  bool
  enabled() const;

  void
  set_direction(dir d, float lo_freq);

  bool
  mixer_lock();

private:
  bsp::chrono    &m_chrono;
  struct rffcx07x m_mixer;
};

/**@} */

} /* namespace satnogs::comms */