/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/bsp/sensor.hpp>
#include <satnogs-comms/emc1702.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/ina322x.hpp>
#include <satnogs-comms/version.hpp>

namespace satnogs::comms
{
/**
 * @class power
 * @brief Manages power supplies and monitors subsystem status.
 *
 * @ingroup power
 *
 * @{
 */
class power
{
public:
  /**
   * @enum subsys
   * @brief Identifies subsystems managed by the power class.
   */
  enum class subsys : uint8_t
  {
    CAN1      = 0, //!< CAN Bus 1
    CAN2      = 1, //!< CAN Bus 2
    RF_5V     = 2, //!< RF 5V supply
    FPGA_5V   = 3, //!< FPGA 5V supply
    CAN1_LPWR = 4, //!< Low-power mode for CAN Bus 1
    CAN2_LPWR = 5, //!< Low-power mode for CAN Bus 2
    UHF       = 6, //!< UHF subsystem
    SBAND     = 7  //!< S-Band subsystem
  };

  /**
   * @enum sensor
   * @brief Identifies power measurement sources.
   */
  enum class sensor : uint8_t
  {
    EFUSES  = 0, //!< Power measured by eFuses
    EMC1702 = 1, //!< Power measured by the EMC1702 sensor
    AVERAGE = 2  //!< Average power from all available sources
  };

  /**
   * @enum channel
   * @brief Identifies voltage and current measurement channels.
   */
  enum class channel : uint8_t
  {
    FPGA    = 1, //!< FPGA voltage/current channel
    RF_5V   = 2, //!< RF 5V voltage/current channel
    DIG_3V3 = 3, //!< Digital 3.3V voltage/current channel
    VIN     = 4, //!< Input voltage channel
    V_BAT   = 5  //!< Battery voltage channel
  };

  /**
   * @brief Power good indicator
   *
   */
  enum class pgood_tp : uint8_t
  {
    RAIL_5V    = 0, //!< 5V rail
    RAIL_FPGA  = 1, //!< FPGA rail
    RAIL_UHF   = 2, //!< UHF rail
    RAIL_SBAND = 3  //!< S-Band rail
  };

  /**
   * @brief Current limit resistor configuration
   *
   * Depending on the mission requirements, SatNOGS-COMMS hardware allows
   * different current limit through appropriate selected resistor values.
   */
  class r_lim
  {
  public:
    r_lim() = delete;

    /**
     * @brief Construct a new r lim object
     *
     * @param dig_3v3 the resistor selected for the digital 3.3V current limit
     * circuit. The value should be in Ohms
     * @param rf_5v the resistor selected for the RF 5V current limit circuit.
     * The value should be in Ohms
     * @param fpga_5v the resistor selected for the FPGA 5V current limit
     * circuit. The value should be in Ohms
     */
    r_lim(uint16_t dig_3v3, uint16_t rf_5v, uint16_t fpga_5v)
        : dig_3v3(dig_3v3), rf_5v(rf_5v), fpga_5v(fpga_5v)
    {
    }

    /*
     * Only the power class should have access on the protected members of this
     * class
     */
    friend power;

  protected:
    uint16_t dig_3v3;
    uint16_t rf_5v;
    uint16_t fpga_5v;
  };
  /**
   * @brief Represents the I/O configuration for the power management system.
   *
   * The `io_conf` class contains references to the hardware interfaces and
   * components required for managing power in the system. This includes GPIOs,
   * ADCs, and sensors used to monitor and control the power subsystems.
   *
   */
  class io_conf
  {
  public:
    bsp::i2c    &mon_i2c;
    bsp::gpio   &rf_5v_en;
    bsp::gpio   &fpga_5v_en;
    bsp::gpio   &can1_en;
    bsp::gpio   &can1_low_pwr;
    bsp::gpio   &can2_en;
    bsp::gpio   &can2_low_pwr;
    bsp::gpio   &rf_5v_pgood;
    bsp::gpio   &fpga_5v_pgood;
    bsp::gpio   &uhf_en;
    bsp::gpio   &uhf_pgood;
    bsp::gpio   &sband_en;
    bsp::gpio   &sband_pgood;
    bsp::adc    &imon_3v3_d;
    bsp::adc    &imon_5v_rf;
    bsp::adc    &imon_fpga;
    bsp::sensor &v_mon;
    r_lim        rlim;
    uint16_t     efuse_adc_current_gain;
  };

  void
  enable(subsys sys, bool en = true);

  bool
  enabled(subsys sys) const;

  bool
  pgood(pgood_tp tp) const;

  float
  voltage(channel sys) const;

  float
  current(channel sys) const;

  float
  get_power(sensor src) const;

  power(const io_conf &io);

private:
  bsp::gpio   &m_rf_5v_en;
  bsp::gpio   &m_fpga_5v_en;
  bsp::gpio   &m_can1_en;
  bsp::gpio   &m_can1_low_pwr;
  bsp::gpio   &m_can2_en;
  bsp::gpio   &m_can2_low_pwr;
  bsp::gpio   &m_pgood_5v;
  bsp::gpio   &m_pgood_fpga;
  emc1702      m_monitor;
  bsp::gpio   &m_uhf_en;
  bsp::gpio   &m_uhf_pgood;
  bsp::gpio   &m_sband_en;
  bsp::gpio   &m_sband_pgood;
  bsp::adc    &m_imon_5v_rf;
  bsp::adc    &m_imon_3v3;
  bsp::adc    &m_imon_fpga;
  bsp::sensor &m_v_mon;
  r_lim        m_r_lim;
  uint16_t     m_efuse_adc_current_gain;
};
/** @} */

} // namespace satnogs::comms
