/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>

namespace satnogs::comms
{

class emmc
{
public:
  /**
   * Direction / interface
   */
  enum class dir
  {
    MCU, /**< MCU interface */
    FPGA /**< FPGA interface */
  };

  emmc(bsp::gpio &en, bsp::gpio &sel, bsp::gpio &rst);

  void
  enable(bool set = false);

  void
  reset(bool set = false);

  bool
  enabled() const;

  void
  set_dir(dir d);

  emmc::dir
  get_dir() const;

private:
  bsp::gpio &m_en_gpio;
  bsp::gpio &m_sel_gpio;
  bsp::gpio &m_rst_gpio;
};

} // namespace satnogs::comms
