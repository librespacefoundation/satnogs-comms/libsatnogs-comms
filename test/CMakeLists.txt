# ##############################################################################
# SatNOGS-COMMS control library
#
# Copyright (C) 2022, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GNU General Public License v3.0 or later
# ##############################################################################

# Build the test project for Zephyr-RTOS
include(ExternalProject)
ExternalProject_Add(
  test
  SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/zephyr-rtos"
  BINARY_DIR "${CMAKE_BINARY_DIR}/test/zephyr-rtos"
  CMAKE_ARGS -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DHW_VERSION=${HW_VERSION}
  PREFIX test
  INSTALL_COMMAND ""
  DEPENDS libsatnogs-comms
  BUILD_ALWAYS TRUE)
