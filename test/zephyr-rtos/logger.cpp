/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "logger.hpp"
#include "bsp/bsp.hpp"
#include "callbacks.hpp"
#include "scoped_lock.hpp"
#include <etl/string_stream.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/drivers/uart.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

LOG_MODULE_DECLARE(satnogscomms);

static auto log_str =
    etl::make_string_with_capacity<CONFIG_LOG_RING_BUFFER_MAX_MSG_LEN>("");

void
logger::boot()
{
  scoped_lock lock(&m_mtx);
  log_str.clear();
  etl::string_stream stream(log_str);
  stream << "SatNOGS-COMMS FW: " << 0 << "." << 0 << "." << 0
         << ", LIB: " << sc::version::lib_major << "." << sc::version::lib_minor
         << "." << sc::version::lib_patch << ", HW: " << sc::version::hw_major
         << "." << sc::version::hw_minor << "." << sc::version::hw_patch
         << " initialized";

  log_unlocked({target::RTT, target::UART, target::RING_BUFFER},
               log_str.c_str());
}

/**
 * @brief Logs an exception to RTT, RING_BUFFER, UART and EMMC
 *
 * @param e The exception object containing error information
 */
void
logger::log(const sc::exception &e)
{
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC}, e);
}

void
logger::log(const char *msg)
{
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC}, msg);
}

void
logger::log(const etl::istring &msg)
{
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC},
      msg.c_str());
}

void
logger::log(std::initializer_list<target> list, const sc::exception &e)
{
  scoped_lock lock(&m_mtx);
  log_unlocked(list, e);
}

void
logger::log(std::initializer_list<target> list, const char *msg)
{
  scoped_lock lock(&m_mtx);
  log_unlocked(list, msg);
}

void
logger::log_unlocked(std::initializer_list<target>    list,
                     const satnogs::comms::exception &e)
{
  for (auto i : list) {
    switch (i) {
    case target::RTT:
      rtt_push(e);
      break;
    case target::UART:
      uart_push(e);
      break;
    case target::RING_BUFFER:
      ring_buffer_push(e);
      break;
    case target::EMMC:
      break;
    default:
      break;
    }
  }
}

void
logger::log_unlocked(std::initializer_list<target> list, const char *msg)
{
  for (auto i : list) {
    switch (i) {
    case target::RTT:
      rtt_push(msg);
      break;
    case target::UART:
      uart_push(msg);
      break;
    case target::RING_BUFFER:
      ring_buffer_push(msg);
      break;
    case target::EMMC:
      break;
    default:
      break;
    }
  }
}

void
logger::rtt_push(const satnogs::comms::exception &e)
{
  LOG_ERR("Exception: %s | Severity: %d", e.what_verbose(),
          static_cast<uint8_t>(e.get_severity()));
}

void
logger::rtt_push(const char *msg)
{
  LOG_INF("%s", msg);
}

void
logger::ring_buffer_push(const satnogs::comms::exception &e)
{
  log_str.clear();
  log_str.append("EXC: ");
  /* Using the ETL we are safe from a possible buffer overflow */
  log_str.append(e.what_terse());
  if (m_ring_buffer.full() == true) {
    m_ring_buffer.pop();
  }
  m_ring_buffer.push(log_str);
}

void
logger::ring_buffer_push(const char *msg)
{
  log_str.clear();
  log_str.append("LOG: ");
  /* Using the ETL we are safe from a possible buffer overflow */
  log_str.append(msg);
  if (m_ring_buffer.full() == true) {
    m_ring_buffer.pop();
  }
  m_ring_buffer.push(log_str);
}

void
logger::uart_push(const satnogs::comms::exception &e)
{
#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
  log_str.clear();
  log_str.append("LOG: ");
  log_str.append(e.what());
  const struct device *uart_log = DEVICE_DT_GET(DT_ALIAS(uartlog));
  uart_tx(uart_log, reinterpret_cast<const uint8_t *>(log_str.c_str()),
          log_str.size(), 100000);
#endif
}

void
logger::uart_push(const char *msg)
{
#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
  auto s = etl::make_string_with_capacity<CONFIG_LOG_RING_BUFFER_MAX_MSG_LEN>(
      "LOG: ");
  s.append(msg);
  const struct device *uart_log = DEVICE_DT_GET(DT_ALIAS(uartlog));
  uart_tx(uart_log, reinterpret_cast<const uint8_t *>(s.c_str()), s.size(),
          100000);
#endif
}

logger::logger() { k_mutex_init(&m_mtx); }
