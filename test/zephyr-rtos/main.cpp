/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "bsp/bsp.hpp"
#include "error_handler.hpp"
#include "etl_profile.h"
#include "version.h"
#include <errno.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/kernel.h>
#include <zephyr/stats/stats.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/sys/util.h>
#include <zephyr/task_wdt/task_wdt.h>
/* Register the log module */
LOG_MODULE_REGISTER(satnogscomms, CONFIG_LOG_DEFAULT_LEVEL);

namespace sc = satnogs::comms;

#if DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(st_stm32_watchdog)
#else
#error "IWDG is not available. Check the device tree configuration"
#endif

const struct device *v_bat_dev = DEVICE_DT_GET(DT_ALIAS(vbat));

const struct gpio_dt_spec led0 =
    GPIO_DT_SPEC_GET_BY_IDX(DT_ALIAS(led0), gpios, 0);
const struct gpio_dt_spec led1 =
    GPIO_DT_SPEC_GET_BY_IDX(DT_ALIAS(led1), gpios, 0);

const struct spi_config radio_spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(radio_spi)),
           .delay = 200}};
const struct device *radio_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi4));

const struct device *i2c_dev = DEVICE_DT_GET(DT_NODELABEL(i2c4));

static const struct adc_dt_spec adc_1_16 =
    ADC_DT_SPEC_GET_BY_NAME(DT_PATH(zephyr_user), imon_3v3_d);
static const struct adc_dt_spec adc_3_0 =
    ADC_DT_SPEC_GET_BY_NAME(DT_PATH(zephyr_user), imon_5v_rf);
static const struct adc_dt_spec adc_3_1 =
    ADC_DT_SPEC_GET_BY_NAME(DT_PATH(zephyr_user), imon_5v_fpga);

spi_bsp radio_spi(radio_spi_dev, radio_spi_cfg);

K_THREAD_STACK_DEFINE(workqueue_thread_stack, 1024);
struct k_work_q main_work_q;

// static void
// task_wdt_callback(int channel_id, void *user_data)
// {
//   printk("Task watchdog channel %d callback", channel_id);

//   /*
//    * If the issue could be resolved, call task_wdt_feed(channel_id) here
//    * to continue operation.
//    *
//    * Otherwise we can perform some cleanup and reset the device.
//    */

//   sys_reboot(SYS_REBOOT_COLD);
// }

gpio_bsp   l0(&led0);
gpio_bsp   l1(&led1);
chrono_bsp chr;
adc_bsp    adc_imon_3v3_d(adc_1_16);
adc_bsp    adc_imon_5v_rf(adc_3_0);
adc_bsp    adc_imon_fpga(adc_3_1);
dac_bsp    dac1(nullptr, 0, 12);
i2c_bsp    sensors_i2c(i2c_dev);
gpio_bsp   x(nullptr);
sensor_bsp sens(v_bat_dev);

int
main(void)
{
  const struct device *hw_wdt_dev = DEVICE_DT_GET(WDT_NODE);
  task_wdt_init(hw_wdt_dev);
  int wdt_id = task_wdt_add(10000, NULL, NULL);
  task_wdt_feed(wdt_id);

  adc_imon_3v3_d.start();
  adc_imon_5v_rf.start();
  adc_imon_fpga.start();

  sc::power::r_lim r_limit(CONFIG_R_LIM_3V3_D, CONFIG_R_LIM_5V_FPGA,
                           CONFIG_R_LIM_5V_RF);

  sc::power::io_conf pwr_cnf = {.mon_i2c       = sensors_i2c,
                                .rf_5v_en      = x,
                                .fpga_5v_en    = x,
                                .can1_en       = x,
                                .can1_low_pwr  = x,
                                .can2_en       = x,
                                .can2_low_pwr  = x,
                                .rf_5v_pgood   = x,
                                .fpga_5v_pgood = x,
                                .uhf_en        = x,
                                .uhf_pgood     = x,
                                .sband_en      = x,
                                .sband_pgood   = x,
                                .imon_3v3_d    = adc_imon_3v3_d,
                                .imon_5v_rf    = adc_imon_5v_rf,
                                .imon_fpga     = adc_imon_fpga,
                                .v_mon         = sens,
                                .rlim          = r_limit,
                                .efuse_adc_current_gain =
                                    CONFIG_EFUSE_ADC_CURRENT_GAIN};

  sc::rf_frontend09::io_conf rffe09_io = {
      .en_agc = x, .agc_vset = dac1, .flt_sel = x};

  sc::rf_frontend24::io_conf rffe24_io = {.en_agc    = x,
                                          .agc_vset  = dac1,
                                          .flt_sel   = x,
                                          .mixer_clk = x,
                                          .mixer_rst = x,
                                          .mixer_enx = x,
                                          .mixer_sda = x,
                                          .chrono    = chr};

  sc::radio::io_conf radio_cnf = {.spi_ctrl  = radio_spi,
                                  .nreset    = x,
                                  .chrono    = chr,
                                  .rffe09_io = rffe09_io,
                                  .rffe24_io = rffe24_io};

  sc::antenna_gpio uhf_ant =
      sc::antenna_gpio(etl::make_string_with_capacity<16>("uhf"),
                       {{.deploy = x, .sense = x}, {.deploy = x, .sense = x}});

  sc::antenna_gpio sband_ant =
      sc::antenna_gpio(etl::make_string_with_capacity<16>("sband"),
                       {{.deploy = x, .sense = x}, {.deploy = x, .sense = x}});

  sc::board::io_conf board_io = {.emmc_en          = x,
                                 .emmc_sel         = x,
                                 .emmc_rst         = x,
                                 .fpga_spi         = radio_spi,
                                 .led0             = l0,
                                 .led1             = l1,
                                 .pwr_io           = pwr_cnf,
                                 .sensors_i2c      = sensors_i2c,
                                 .alert_t_pa_uhf   = x,
                                 .alert_t_pa_sband = x,
                                 .radio_io         = radio_cnf,
                                 .chrono           = chr,
                                 .fpga_done        = x,
                                 .uhf_antenna      = uhf_ant,
                                 .sband_antenna    = sband_ant};

  msgq<sc::radio::rx_msg, 2> rx_msgq;

  sc::board::params board_params = {
      .radio_params = {.uhf_len   = {.tx = 448, .rx = 256},
                       .sband_len = {.tx = 448, .rx = 256}},
      .fpga_hw      = sc::fpga::hw::ALINX_AC7Z020,
      .rx_msgq      = rx_msgq};

  sc::board::init(board_io, board_params);

  auto &radio = sc::board::get_instance().radio();
  auto &pwr   = sc::board::get_instance().power();

  radio.enable();
  radio.enable(sc::radio::interface::UHF, false);
  radio.enable(sc::radio::interface::SBAND, false);

  auto &fpga = sc::board::get_instance().fpga();
  fpga.enable();
  /*
   * Initialize the main work queue that can accept workers. This a
   * good practice instead of executing directly stuff into the timer.
   *
   * Timers on Zephyr operate in interrupt context.
   */
  k_work_queue_start(&main_work_q, workqueue_thread_stack,
                     K_THREAD_STACK_SIZEOF(workqueue_thread_stack), 5, NULL);

  pwr.current(sc::power::channel::DIG_3V3);
  pwr.current(sc::power::channel::RF_5V);
  pwr.current(sc::power::channel::FPGA);
  pwr.get_power(sc::power::sensor::EFUSES);
  pwr.get_power(sc::power::sensor::EMC1702);
  pwr.get_power(sc::power::sensor::AVERAGE);

  while (1) {
    try {
      task_wdt_feed(wdt_id);
      k_msleep(1000);
    } catch (const sc::exception &e) {
      error_handler::get_instance().handle_err(e);
    }
  }
  return 0;
}
