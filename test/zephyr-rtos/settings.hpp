/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include <satnogs-comms/radio.hpp>
#include <zephyr/kernel.h>

class settings
{
public:
  static constexpr const char *setting_boot_count    = "/lfs1/boot_cnt";
  static constexpr const char *setting_uhf_tx_freq   = "/lfs1/uhf-tx-freq";
  static constexpr const char *setting_uhf_rx_freq   = "/lfs1/uhf-rx-freq";
  static constexpr const char *setting_sband_tx_freq = "/lfs1/sband-tx-freq";
  static constexpr const char *setting_sband_rx_freq = "/lfs1/sband-rx-freq";
  static constexpr const char *setting_uhf_tx_en     = "/lfs1/uhf-tx-en";
  static constexpr const char *setting_sband_tx_en   = "/lfs1/sband-tx-en";
  static constexpr const char *setting_tlm_period    = "/lfs1/tlm-period";
  static constexpr const char *setting_uhf_tx_gain   = "/lfs1/uhf-tx-gain";
  static constexpr const char *setting_sband_tx_gain = "/lfs1/sband-tx-gain";
  static constexpr const char *setting_io_wdg_period_min =
      "/lfs1/io-wdg-period-min";
  static constexpr const char *setting_errno = "/lfs1/errno";
  static constexpr const char *setting_reset_errno_count =
      "/lfs1/reset-errno-count"; // Count for resets due to the same error

  static settings &
  get_instance()
  {
    static settings instance;
    return instance;
  }

  /* Singleton */
  settings(settings const &) = delete;

  void
  operator=(settings const &) = delete;

  uint32_t
  boot_cnt();
  void
  incr_boot_cnt();

  float
  tx_freq(satnogs::comms::radio::interface iface);
  void
  set_tx_freq(satnogs::comms::radio::interface iface, float freq);

  float
  rx_freq(satnogs::comms::radio::interface iface);
  void
  set_rx_freq(satnogs::comms::radio::interface iface, float freq);

  void
  incr_rst_errno_cnt();

  uint32_t
  rst_errno_cnt();

  void
  set_err_num(uint32_t err_msg);

  uint32_t
  err_num();

  bool
  tx_enabled(satnogs::comms::radio::interface iface);
  void
  set_tx_enable(satnogs::comms::radio::interface iface, bool en);

  float
  tx_gain(satnogs::comms::radio::interface iface);
  void
  set_tx_gain(satnogs::comms::radio::interface iface, float gain);

  bool
  deployed();

  uint32_t
  periodic_tlm_secs();
  void
  set_periodic_tlm_secs(uint32_t secs);

  void
  set_io_wdg_period_min(uint32_t mins);

  uint32_t
  io_wdg_period_mins();

protected:
  settings();

private:
  mutable struct k_mutex m_mtx;

  int
  read32(const char *fname, uint32_t *x);

  int
  write32(const char *fname, uint32_t x);

  int
  readf(const char *fname, float *x);

  int
  writef(const char *fname, float x);
};
