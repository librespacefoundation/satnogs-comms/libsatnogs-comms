#include "error_handler.hpp"
#include <zephyr/drivers/hwinfo.h>
#include <zephyr/sys/reboot.h>

namespace sc = satnogs::comms;

/**
 * @brief Asserts an error and triggers a failure.
 *
 * This method triggers an assertion failure using `ETL_ASSERT_FAIL`
 * for the provided exception. Can be overridden.
 *
 * @param error The exception object containing error information.
 */
void
error_handler::assert_error(const sc::exception &error)
{
  assert_error(0, error);
}

/**
 * @brief Asserts an error based on a condition.
 *
 * This method triggers an assertion only if the condition is false,
 * using `ETL_ASSERT`. Can be overridden.
 *
 * @param condition The condition to evaluate (false triggers an assertion).
 * @param error The exception object containing error information.
 */
void
error_handler::assert_error(bool condition, const sc::exception &error)
{
  ETL_ASSERT(condition, error);
}

uint32_t
error_handler::hwinfo_reset_cause() const
{
  return m_hw_reset_cause;
}

void
error_handler::get_reset_cause()
{
  uint32_t cause;
  hwinfo_get_reset_cause(&cause);
  hwinfo_clear_reset_cause();
  m_hw_reset_cause = cause;
  return;
}

void
error_handler::system_reboot() const
{
  /* Wait for the WDT to reset the system */
  k_sleep(K_MINUTES(30));
  /* If there is any issue with the WDT, reboot manually after 30 minutes */
  sys_reboot(SYS_REBOOT_COLD);
  return;
}

void
error_handler::handle_err(const satnogs::comms::exception &e)
{
  // TODO
}

void
error_handler::error_log(const satnogs::comms::exception &e) const
{
  // TODO
}

error_handler::error_handler() : m_hw_reset_cause(0) { get_reset_cause(); }
