/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "settings.hpp"
#include "scoped_lock.hpp"
#include <zephyr/fs/fs.h>
#include <zephyr/fs/littlefs.h>
#include <zephyr/storage/flash_map.h>

using radio = satnogs::comms::radio;

FS_FSTAB_DECLARE_ENTRY(DT_NODELABEL(lfs1));
struct fs_mount_t *mountpoint = &FS_FSTAB_ENTRY(DT_NODELABEL(lfs1));

settings::settings()
{
  k_mutex_init(&m_mtx);

  /*
   * If the user has selected it, erase the entire settings area. The settings
   * subsystem then will handle the defaults handling
   */
#if CONFIG_ERASE_SETTINGS
  /* If the user has selected it, erase the entire settings area. The settings
   * subsystem then will handle the defaults handling
   */
  const struct flash_area *pfa;
  flash_area_open(FIXED_PARTITION_ID(lfs1_partition), &pfa);
  flash_area_erase(pfa, 0, pfa->fa_size);
  flash_area_close(pfa);
#endif

  /*
   * If mount fails, the sub-sequent read/write operations will fail and the
   * settings subsystem will provide defaults
   */
  fs_mount(mountpoint);
}

int
settings::read32(const char *fname, uint32_t *x)
{
  uint32_t         val = 0;
  struct fs_file_t file;
  int              ret;

  fs_file_t_init(&file);
  ret = fs_open(&file, fname, FS_O_READ);
  if (ret) {
    return ret;
  }

  ret = fs_read(&file, &val, sizeof(val));
  if (ret < 0) {
    fs_close(&file);
    return ret;
  }

  ret = fs_close(&file);
  if (ret) {
    return ret;
  }
  *x = val;
  return 0;
}

int
settings::write32(const char *fname, uint32_t x)
{
  struct fs_file_t file;
  int              ret;

  fs_file_t_init(&file);
  ret = fs_open(&file, fname, FS_O_CREATE | FS_O_RDWR);
  if (ret && ret != -ENOENT) {
    return ret;
  }

  ret = fs_write(&file, &x, sizeof(x));
  if (ret < 0) {
    fs_close(&file);
    return ret;
  }

  return fs_close(&file);
}

int
settings::readf(const char *fname, float *x)
{
  uint32_t val = 0;
  int      ret = read32(fname, &val);
  if (ret) {
    return ret;
  }
  memcpy(x, &val, sizeof(float));
  return 0;
}

int
settings::writef(const char *fname, float x)
{
  uint32_t val = 0;
  memcpy(&val, &x, sizeof(val));
  return write32(fname, val);
}

uint32_t
settings::boot_cnt()
{
  scoped_lock lock(&m_mtx);
  uint32_t    cnt = 0;
  int         ret = read32(setting_boot_count, &cnt);
  if (ret == -ENOENT) {
    /* Apply default value */
    write32(setting_boot_count, 0);
    return 0;
  }
  return cnt;
}

void
settings::incr_boot_cnt()
{
  scoped_lock lock(&m_mtx);
  uint32_t    cnt = boot_cnt();
  cnt++;
  write32(setting_boot_count, cnt);
}

float
settings::tx_freq(satnogs::comms::radio::interface iface)
{
  scoped_lock lock(&m_mtx);
  float       x = 0.0f;
  if (iface == radio::interface::UHF) {
    int ret = readf(setting_uhf_tx_freq, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_uhf_tx_freq, CONFIG_UHF_TX_FREQ_HZ);
      return CONFIG_UHF_TX_FREQ_HZ;
    }
  } else {
    int ret = readf(setting_sband_tx_freq, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_sband_tx_freq, CONFIG_SBAND_TX_FREQ_HZ);
      return CONFIG_SBAND_TX_FREQ_HZ;
    }
  }
  return x;
}

void
settings::set_tx_freq(satnogs::comms::radio::interface iface, float freq)
{
  scoped_lock lock(&m_mtx);
  writef(iface == radio::interface::UHF ? setting_uhf_tx_freq
                                        : setting_sband_tx_freq,
         freq);
}

float
settings::rx_freq(satnogs::comms::radio::interface iface)
{
  scoped_lock lock(&m_mtx);
  float       x = 0.0f;
  if (iface == radio::interface::UHF) {
    int ret = readf(setting_uhf_rx_freq, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_uhf_rx_freq, CONFIG_UHF_RX_FREQ_HZ);
      return CONFIG_UHF_RX_FREQ_HZ;
    }
  } else {
    int ret = readf(setting_sband_rx_freq, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_sband_rx_freq, CONFIG_SBAND_RX_FREQ_HZ);
      return CONFIG_SBAND_RX_FREQ_HZ;
    }
  }
  return x;
}

void
settings::set_rx_freq(satnogs::comms::radio::interface iface, float freq)
{
  scoped_lock lock(&m_mtx);
  if (iface == radio::interface::UHF) {
    writef(setting_uhf_rx_freq, freq);
  } else {
    writef(setting_sband_rx_freq, freq);
  }
}

void
settings::incr_rst_errno_cnt()
{
  scoped_lock lock(&m_mtx);
  uint32_t    cnt = rst_errno_cnt();
  cnt++;
  write32(setting_reset_errno_count, cnt);
}

uint32_t
settings::rst_errno_cnt()
{
  scoped_lock lock(&m_mtx);
  uint32_t    cnt = 0;
  int         ret = read32(setting_reset_errno_count, &cnt);
  if (ret == -ENOENT) {
    /* Apply default value */
    write32(setting_reset_errno_count, 0);
    return 0;
  }
  return cnt;
}

void
settings::set_err_num(uint32_t err_msg)
{
  scoped_lock lock(&m_mtx);
  write32(setting_errno, err_msg);
}

uint32_t
settings::err_num()
{
  scoped_lock lock(&m_mtx);
  uint32_t    err_msg = 0;
  int         ret     = read32(setting_errno, &err_msg);
  if (ret == -ENOENT) {
    write32(setting_errno, 0);
  }
  return err_msg;
}

bool
settings::tx_enabled(satnogs::comms::radio::interface iface)
{
  scoped_lock lock(&m_mtx);
  uint32_t    x = 0;
  if (iface == radio::interface::UHF) {
    int ret = read32(setting_uhf_tx_en, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      write32(setting_uhf_tx_en, 1);
      return 1;
    }
  } else {
    int ret = read32(setting_sband_tx_en, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_sband_tx_en, 1);
      return 1;
    }
  }
  return x;
}

void
settings::set_tx_enable(satnogs::comms::radio::interface iface, bool en)
{
  scoped_lock lock(&m_mtx);
  uint32_t    x = en;
  write32(iface == radio::interface::UHF ? setting_uhf_tx_en
                                         : setting_sband_tx_en,
          x);
}

float
settings::tx_gain(radio::interface iface)
{
  scoped_lock lock(&m_mtx);
  float       x;
  switch (iface) {
  case radio::interface::UHF: {
    int ret = readf(setting_uhf_tx_gain, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_uhf_tx_gain, CONFIG_UHF_TX_GAIN);
      return x;
    }
  }
    return x;
  case radio::interface::SBAND: {
    int ret = readf(setting_sband_tx_gain, &x);
    if (ret == -ENOENT) {
      /* Apply default value */
      writef(setting_sband_tx_gain, CONFIG_SBAND_TX_GAIN);
      return x;
    }
  }
    return x;
  default:
    return std::numeric_limits<float>::signaling_NaN();
  }
}

void
settings::set_tx_gain(radio::interface iface, float gain)
{
  scoped_lock lock(&m_mtx);
  switch (iface) {
  case radio::interface::UHF: {
    writef(setting_uhf_tx_gain, gain);
  } break;
  case radio::interface::SBAND: {
    writef(setting_sband_tx_gain, gain);
  } break;
  default:
    return;
  }
}

bool
settings::deployed()
{
  return boot_cnt() > 0;
}

uint32_t
settings::periodic_tlm_secs()
{
  scoped_lock lock(&m_mtx);
  uint32_t    period = CONFIG_TLM_PERIOD_SECS;
  int         ret    = read32(setting_tlm_period, &period);
  if (ret == -ENOENT) {
    /* Apply default value */
    write32(setting_tlm_period, CONFIG_TLM_PERIOD_SECS);
    return CONFIG_TLM_PERIOD_SECS;
  }
  return period;
}

void
settings::set_periodic_tlm_secs(uint32_t secs)
{
  scoped_lock lock(&m_mtx);
  write32(setting_tlm_period, secs);
}

void
settings::set_io_wdg_period_min(uint32_t mins)
{
  scoped_lock lock(&m_mtx);
  write32(setting_io_wdg_period_min, mins);
}

uint32_t
settings::io_wdg_period_mins()
{
  scoped_lock lock(&m_mtx);
  uint32_t    period = CONFIG_IO_WDG_PERIOD_MINS;
  int         ret    = read32(setting_io_wdg_period_min, &period);
  if (ret == -ENOENT) {
    write32(setting_io_wdg_period_min, CONFIG_IO_WDG_PERIOD_MINS);
    return CONFIG_IO_WDG_PERIOD_MINS;
  }
  return period;
}
