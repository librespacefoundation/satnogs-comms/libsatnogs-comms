/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <etl/circular_buffer.h>
#include <etl/string.h>
#include <initializer_list>
#include <satnogs-comms/exception.hpp>
#include <string>
#include <zephyr/devicetree.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

class logger
{
public:
  /**
   * @brief Enumeration for targets to send logs to.
   */
  enum class target
  {
    RTT         = 0,
    UART        = 1,
    RING_BUFFER = 2,
    EMMC        = 3,
  };

  logger(logger const &) = delete;

  void
  operator=(logger const &) = delete;

  static logger &
  get_instance()
  {
    static logger instance;
    return instance;
  }

  void
  boot();

  void
  log(std::initializer_list<target> list, const satnogs::comms::exception &e);

  void
  log(std::initializer_list<target> list, const char *msg);

  void
  log(const satnogs::comms::exception &e);

  void
  log(const char *msg);

  void
  log(const etl::istring &msg);

private:
  void
  log_unlocked(std::initializer_list<target>    list,
               const satnogs::comms::exception &e);

  void
  log_unlocked(std::initializer_list<target> list, const char *msg);

  void
  rtt_push(const satnogs::comms::exception &e);

  void
  rtt_push(const char *msg);

  void
  ring_buffer_push(const satnogs::comms::exception &e);

  void
  ring_buffer_push(const char *msg);

  void
  uart_push(const satnogs::comms::exception &e);

  void
  uart_push(const char *msg);

  logger();

  etl::circular_buffer<etl::string<CONFIG_LOG_RING_BUFFER_MAX_MSG_LEN>,
                       CONFIG_LOG_RING_BUFFER_MSGS>
                         m_ring_buffer;
  mutable struct k_mutex m_mtx;
};
