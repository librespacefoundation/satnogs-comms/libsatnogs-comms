/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

/ {
    aliases {
        gnss = &gnss_usart1;
        uartgnss = &usart1;
    };
};


/* Bus B:
 *
 * Include this overlay to use the UART port labeled as UART_B on the
 * satnogs-comms board, which corresponds to USART1 in the STM32 pinout. 
 */

&usart1  {
    pinctrl-0 = <&usart1_tx_pa9 &usart1_rx_pa10>;
    pinctrl-names = "default";
    current-speed = <9600>;
    status = "okay";
    gnss_usart1: gnss-nmea-generic-2 {
        compatible = "gnss-nmea-generic";
    };
};


