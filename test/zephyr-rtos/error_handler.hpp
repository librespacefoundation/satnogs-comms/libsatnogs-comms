/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <etl/error_handler.h>
#include <satnogs-comms/exception.hpp>

class error_handler
{
public:
  static error_handler &
  get_instance()
  {
    static error_handler instance;
    return instance;
  }

  /* Singleton */
  error_handler(error_handler const &) = delete;
  void
  operator=(error_handler const &) = delete;

  /**
   * @brief Logs error details and throws an exception with ETL_ASSERT().
   *
   * If `CONFIG_DEBUG_S_C` is enabled, Zephyr __ASSERT() is used instead
   *
   * @param error The exception object containing error info.
   */
  void
  assert_error(const satnogs::comms::exception &e);

  /**
   * @brief Logs error details and throws an exception with ETL_ASSERT() based
   * on the condition.
   *
   * If `CONFIG_DEBUG_S_C` is enabled, Zephyr __ASSERT() is used instead.
   * Triggers if the condition is false.
   *
   * @param condition The condition to evaluate.
   * @param error The exception object containing error info.
   */
  void
  assert_error(bool condition, const satnogs::comms::exception &e);

  /**
   * @brief Get the HW reason of reset
   *
   * @return The HW reason of reset
   */
  uint32_t
  hwinfo_reset_cause() const;

  /**
   * @brief Enter a busy loop and wait for the WDT to reset the system
   *
   * If the WDT fails to reset the system, reboot manually after 30 minutes
   * @return The HW reason of reset
   */
  void
  system_reboot() const;

  /**
   * @brief Handle error based on severity level
   *
   * @param error The exception object containing error information.
   */
  void
  handle_err(const satnogs::comms::exception &e);

  /**
   * @brief Specifies the post-action to take after an error
   * is asserted by ETL error log.
   *
   * @param error The exception object containing error information.
   */
  void
  error_log(const satnogs::comms::exception &e) const;

  /**
   * @brief Asserts an error with file and line number information.
   *
   * This templated method creates an error object of type `T` using the
   * provided file and line information, then triggers an assertion.
   *
   * @tparam T The type of exception to instantiate.
   * @param file The source file where the error occurred.
   * @param line The line number where the error occurred.
   */
  template <typename T>
  void
  assert_error(const char *file, int line)
  {
    T error(file, line);
    assert_error(error);
  }

  /**
   * @brief Asserts an error based on a condition, with file and line
   * information.
   *
   * This templated method creates an error object of type `T` using the
   * provided file and line information, then triggers an assertion only if
   * the condition is false.
   *
   * @tparam T The type of exception.
   * @param condition The condition to evaluate.
   * @param file The source file where the error occurred.
   * @param line The line number where the error occurred.
   */
  template <typename T>
  void
  assert_error(bool condition, const char *file, int line)
  {
    T error(file, line);
    assert_error(condition, error);
  }

protected:
  error_handler();

private:
  uint32_t m_hw_reset_cause;

  void
  get_reset_cause();
};
