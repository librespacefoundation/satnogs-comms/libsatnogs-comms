/**
@defgroup libsatnogs-comms SatNOGS-COMMS Control Library
@brief SatNOGS-COMMS Control Library is an interface library that controls the
SatNOGS-COMMS transceiver.
This library relies a peripheral abstraction layer, allowing users of the
SatNOGS-COMMS to build
their own firmware with minimal effort.
@{
@}

@defgroup rffe Radio and RF-frontend
@ingroup libsatnogs-comms

@brief Radio and RF-frontend control and management

The SatNOGS-COMMS board offers two distinct RF interfaces: one operating on the UHF
band and the other on the S-band.

To interact with these interfaces, the following classes are provided:
 - satnogs::comms::radio()
 - satnogs::comms::rf_frontend09()
 - satnogs::comms::rf_frontend24()

For most users, the satnogs::comms::radio() class is sufficient for basic RF operations.
However, for more specialized use cases requiring precise control over RF parameters,
the satnogs::comms::rf_frontend09() and satnogs::comms::rf_frontend24() classes offer the necessary tools

@{
@}

@defgroup exceptions Exceptions subsystem
@brief Exception Handling in SatNOGS-COMMS
@ingroup libsatnogs-comms

The SatNOGS-COMMS control library leverages exceptions to gracefully handle abnormal behaviors,
providing greater flexibility and clarity in error management.
While this approach might slightly increase firmware size,
the STM32H743 MCU offers enough flash memory to accommodate most applications.

Exceptions in SatNOGS-COMMS vary in severity,
and their occurrence may not necessarily compromise the overall operational status of the subsystem.
For instance, a failed frame transmission due to noise-induced SPI communication errors might be recoverable through retries.

However, more severe exceptions may necessitate additional recovery actions, such as:
- Board Reboot: A complete system restart to address critical issues.
- Subsystem Power Cycle: Resetting specific subsystems to resolve isolated problems.
@{
@}

@defgroup bsp Board Support Package (BSP)
@brief Provides a peripheral abstraction layer for the SatNOGS-COMMS board.
@ingroup libsatnogs-comms

The Board Support Package (BSP) is a part of the SatNOGS-COMMS Control Library,
that offers a peripheral abstraction layer for the SatNOGS-COMMS board. The official software
of the transceiver board utilizes this library and Zephyr-RTOS.
However, the user is not bound to choose the Zephyr-RTOS to build their own firmware.

@{
@}

@defgroup dsp Digital Signal Processing (DSP)
@brief Provides methods for CCSDS scrambling and descrambling.
@ingroup libsatnogs-comms

This group provides the implementation of the CCSDS scrambler and descrambler
based on the randomization procedure with the initial seed as defined by
CCSDS 231.0-B-3.

@{
@}

@defgroup power Power subsystem
@brief Provides methods to retrieve information and control power-related subsystems
@ingroup libsatnogs-comms

This module includes functionalities for monitoring power status, obtaining readings
from power sensors, accessing voltage and current measurements, and checking power-good
(PGOOD) indicators. It also provides methods for enabling or disabling the power supply
to various subsystems.

@{
@}

@defgroup temperature Temperature subsystem
@brief Provides methods to retrieve sensor readings related to temperature
@ingroup libsatnogs-comms

@{
@}

@defgroup antenna Antenna subsystem
@brief Antenna deployment and sensing control
@ingroup libsatnogs-comms

SatNOGS-COMMS provides a flexible and unified way to control and get feedback from a variety of different antennas.
The class \ref antenna provides a generic API allowing for deployment mechanisms and procedures to be implemented at
the firmware regardless of the underlying hardware control of the antenna elements.

@{
@}

*/