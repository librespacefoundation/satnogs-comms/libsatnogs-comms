/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/rf_frontend09.hpp>

namespace satnogs::comms
{

/**
 * @brief Construct a new rf frontend09::rf frontend09 object that controls the
 * UHF interface
 *
 * @param init_params the initialization parameters
 * @param io the IO configuration for controlling  the
 * RF-frontend first stage RX gain as well as the filter selection circuit
 * @param pwr reference to power subsystem, used to power up/down the various
 * subsystems of the UHF RF frontend
 */
rf_frontend09::rf_frontend09(const params &init_params, io_conf &io, power &pwr)
    : rf_frontend(init_params, std::move(io), pwr)
{
}

/**
 * Enables the basic subsystems of the RF frontend. Subsystems that are
 * used for a specific direction (RX or TX) are explicitly disabled to reduce
 * power consumption.
 *
 * Users may have to call the set_direction() method to enable the corresponding
 * subsystems such as PA or LNA.
 *
 * @param set true to enable, false to disable.
 */
void
rf_frontend09::enable(bool set)
{
  m_pwr.enable(power::subsys::UHF, set);
}

/**
 * @brief Checks if the UHF RF frontend is enabled
 *
 * @return true if it is enabled
 * @return false  if it is disabled
 */
bool
rf_frontend09::enabled() const
{
  return m_pwr.enabled(power::subsys::UHF);
}

/**
 * @brief This method is used to change the direction (TX/RX) of the
 * RF-frontend. Any components that are not used by a specific direction are
 * disabled to reduce the power consumption footprint.
 *
 * @param d the desired direction
 * @param lo_freq Not used by the UHF iinterface
 *
 * @warning When the RF frontend is set to TX mode all gain stages of the RX are
 * disabled. Users should set again the proper gain settings. In other words, of
 * set_direction(dir::RX) is called, it should be followed by an appropriate
 * call of set_rx_gain()
 */
void
rf_frontend09::set_direction(dir d, float lo_freq)
{
  switch (d) {
  case dir::RX:
    m_dir = d;
    break;
  case dir::TX:
    m_dir = d;
    m_agc.enable(false);
    m_dac.stop();
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

} // namespace satnogs::comms
