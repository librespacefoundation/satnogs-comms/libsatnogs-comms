/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024-2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/antenna.hpp>

namespace satnogs::comms
{

/**
 * @brief Construct a new generic antenna object
 *
 * @param name the name of the antenna
 * @param nelems the number of elements that are controlled/deployed
 * independently. (e.g 2 for dipole, 4 for turnstile, etc)
 */
antenna::antenna(const etl::istring &name, uint32_t nelems)
    : m_name(name), m_nelems(nelems)
{
}

/**
 * @brief Returns antenna deployment status.
 *
 * @param idx An antenna index. Should be in the range [0, \ref nelems()-1]
 * @return true if the the antenna is deployed, false otherwise
 */
bool
antenna::detect(uint32_t idx)
{
  return false;
}

/**
 * @brief Deploy the antenna
 *
 * @param idx An antenna index. Should be in the range [0, \ref nelems()-1]
 * @param en true to deploy, false to stop trying to deploy.
 */
void
antenna::deploy(uint32_t idx, bool en)
{
}

/**
 * @brief Retrieves the name of the antenna.
 *
 * @return The name of the antenna as a string.
 */
const char *
antenna::name() const
{
  return m_name.c_str();
}

/**
 * @brief Retrieves the number of available elements of the antenna
 *
 * @return uint32_t the number of elements available on this antenna
 */
uint32_t
antenna::nelems() const
{
  return m_nelems;
}

} // namespace satnogs::comms
