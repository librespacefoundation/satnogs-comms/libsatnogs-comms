/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/lna.hpp>

namespace satnogs::comms
{

/**
 * Creates a generic LNA handler
 * @param name the name of the LNA
 * @param io the GPIO to enable/disable the LNA
 */
lna::lna(const etl::istring &name, bsp::gpio &io) : m_gpio(io), m_name(name)
{
  enable(false);
}

const etl::istring &
lna::name() const
{
  return m_name;
}

/**
 * Enable/disable the LNA
 * @param set set to true to enable the LNA, false to disable.
 */
void
lna::enable(bool set)
{
  m_gpio.set(set);
}

/**
 * Toggles the state of the LNA
 */
void
lna::toggle()
{
  m_gpio.toggle();
}

/**
 *
 * @return true of the LNA is enabled, false otherwise
 */
bool
lna::enabled() const
{
  return m_gpio.get();
}

} // namespace satnogs::comms
