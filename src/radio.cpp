/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <etl/binary.h>
#include <etl/crc.h>
#include <etl/vector.h>
#include <satnogs-comms/board.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/dsp/ccsds.hpp>
#include <satnogs-comms/power.hpp>
#include <satnogs-comms/radio.hpp>

namespace satnogs::comms
{

static radio::rx_msg rx_msg;
// FIXME This has to change. It is not safe for both interfaces
static uint8_t tx_pdu[AT86RF215_MAX_PDU];

/**
 * @brief Modulation index translation map. Each index corresponds to a @ref
 * at86rf215_fsk_midx_t setting
 *
 */
static constexpr std::array<float, 8> fsk_mod_idxs = {
    0.375f, 0.5f, 0.75f, 1.0f, 1.25f, 1.5f, 1.75f, 2.0f};

/**
 * @brief FSK Shaping filter bandwidth translation map.  Each index corresponds
 * to a @ref at86rf215_fsk_bt_t setting
 *
 */
static constexpr std::array<float, 4> fsk_bt = {0.5f, 1.0f, 1.5f, 2.0f};

/* The C++ way. This is the way! */
template <typename T, const size_t IDX,
          std::enable_if_t<std::is_integral_v<T>, bool> = true>
static constexpr T
bit()
{
  static_assert(IDX < sizeof(T) * 8, "The operation will overflow!");
  return static_cast<T>(1) << IDX;
}

/* Re-implementation of the AT86RF215 weak functions */
extern "C" {
int
at86rf215_set_rstn(struct at86rf215 *h, uint8_t enable)
{
  if (h->rst_gpio_dev) {
    bsp::gpio *rst_gpio_dev = static_cast<bsp::gpio *>(h->rst_gpio_dev);
    rst_gpio_dev->set(enable);
  }
  return AT86RF215_OK;
}

int
at86rf215_set_seln(struct at86rf215 *h, uint8_t enable)
{
  if (h->cs_gpio_dev) {
    bsp::gpio *cs_gpio_dev = static_cast<bsp::gpio *>(h->cs_gpio_dev);
    /*
     * NOTE: AT86RF215 uses physical pin level logic for the CS
     */
    cs_gpio_dev->set_raw(enable);
  }
  return AT86RF215_OK;
}

void
at86rf215_delay_us(struct at86rf215 *h, uint32_t us)
{
  bsp::chrono *chrono_dev = static_cast<bsp::chrono *>(h->user_dev0);
  chrono_dev->delay_us(us);
}

size_t
at86rf215_get_time_ms(struct at86rf215 *h)
{
  bsp::chrono *chrono_dev = static_cast<bsp::chrono *>(h->user_dev0);
  return chrono_dev->time_ms();
}

int
at86rf215_spi_read(struct at86rf215 *h, uint8_t *out, const uint8_t *in,
                   size_t tx_len, size_t rx_len)
{
  bsp::spi *spi_dev = static_cast<bsp::spi *>(h->spi_dev);
  spi_dev->read(out, in, tx_len, rx_len);
  return AT86RF215_OK;
}

int
at86rf215_spi_write(struct at86rf215 *h, const uint8_t *in, size_t len)
{
  bsp::spi *spi_dev = static_cast<bsp::spi *>(h->spi_dev);
  spi_dev->write(in, len);
  return AT86RF215_OK;
}

int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs)
{
  radio *r = reinterpret_cast<radio *>(h->user_dev1);
  /* Check if a frame has been received */
  if (bbc0_irqs & bit<uint8_t, 1>()) {

    // FIXME USE DMA!
    at86rf215_rx_frame(h, AT86RF215_RF09, rx_msg.pdu,
                       r->rx_len(radio::interface::UHF));
    rx_msg.info.len   = r->rx_len(radio::interface::UHF);
    rx_msg.info.iface = radio::interface::UHF;
    /*
     * Get the RSSI of the received frame. According to the datasheet this
     * should be the EDV register
     */
    int ret = at86rf215_get_edv(h, AT86RF215_RF09, &rx_msg.info.rssi);
    if (ret) {
      rx_msg.info.rssi = std::nanf("nan");
    }

    ret = r->m_rxq.put_isr(rx_msg);
    if (ret) {
      /* FIFO is full probably */
      r->incr_rx_drop_frames(radio::interface::UHF);
    }

    /* After the RXFE the AT86 does not enter RX mode */
    at86rf215_set_cmd(h, AT86RF215_CMD_RF_RX, AT86RF215_RF09);
  }

  /* Check if a frame has been received */
  if (bbc1_irqs & bit<uint8_t, 1>()) {
    // FIXME USE DMA!
    at86rf215_rx_frame(h, AT86RF215_RF24, rx_msg.pdu,
                       r->rx_len(radio::interface::SBAND));
    rx_msg.info.len   = r->rx_len(radio::interface::SBAND);
    rx_msg.info.iface = radio::interface::SBAND;
    /*
     * Get the RSSI of the received frame. According to the datasheet this
     * should be the EDV register
     */
    int ret = at86rf215_get_edv(h, AT86RF215_RF24, &rx_msg.info.rssi);
    if (ret) {
      rx_msg.info.rssi = std::nanf("nan");
    }

    ret = r->m_rxq.put_isr(rx_msg);
    if (ret) {
      /* FIFO is full probably */
      r->incr_rx_drop_frames(radio::interface::SBAND);
    }

    /* After the RXFE the AT86 does not enter RX mode */
    at86rf215_set_cmd(h, AT86RF215_CMD_RF_RX, AT86RF215_RF24);
  }
  return AT86RF215_OK;
}
}

/**
 * @brief Converts the radio interface to the AT86RF215 equivalent
 *
 * @param iface radio interface
 * @return at86rf215_radio_t the AT86RF215 interface
 */
static at86rf215_radio_t
iface_to_at86_radio(radio::interface iface)
{
  return iface == radio::interface::UHF ? AT86RF215_RF09 : AT86RF215_RF24;
}

/**
 * @brief Construct a new radio::radio object
 *
 * @param p configuration parameters
 * @param cnf IO configuration for controlling the peripherals of the radio
 * @param pwr reference to the power subsystem. Necessary to power up/down the
 * radio components depending on the user calls
 * @param rx_msgq a thread safe message queue that will hold the RX messages
 * from both UHF and S-Band interfaces at they received and validated
 */
radio::radio(const params &p, const io_conf &cnf, power &pwr,
             bsp::imsgq<rx_msg> &rx_msgq)
    : m_frame_len{p.uhf_len, p.sband_len},
      m_spi(cnf.spi_ctrl),
      m_nrst(cnf.nreset),
      m_tx_mixer_params(p.tx_mixer_params),
      m_rx_mixer_params(p.rx_mixer_params),
      m_iface_enabled{false, false},
      m_rffe09(p.rffe09_params, cnf.rffe09_io, pwr),
      m_rffe24(p.rffe24_params, cnf.rffe24_io, pwr),
      m_chrono(cnf.chrono),
      m_pwr(pwr),
      m_rxq(rx_msgq)
{
  bsp_setup();
  at86rf215_enable(false);
}

/**
 * @brief The IRQ handler of the AT86RF215 IRQ
 *
 * @attention The handling of this static method is platform dependent. Users
 * should call this method inside the IRQ handling routine of the target
 * platform
 */
void
radio::trx_irq_handler()
{
  auto &r = board::get_instance().radio();
  at86rf215_irq_callback(&r.m_at86);
}

/**
 * @brief Enable/disable the radio chain.
 * @attention This method does not enable the RF front-ends.
 * It powers up the RF 5V power supply, allowing then for each RF front-end
 * to be enabled separately.
 *
 * @note To avoid accidental activation for any front-end after an
 * enable/disable cycle, when a disable is requested,
 * the RF front-ends are explicitly disabled
 *
 * @param yes true to enable, false to disable
 */
void
radio::enable(bool yes)
{
  if (yes) {
    m_pwr.enable(power::subsys::RF_5V, true);
    at86rf215_enable(true);
    at86rf215_irq_clear(&m_at86);
  } else {
    /*
     * Disabling the whole chain is critical. Set explicitly the RF front-ends
     * as disabled and disable the corresponding power supplies
     */
    m_iface_enabled[static_cast<uint8_t>(interface::UHF)]   = false;
    m_iface_enabled[static_cast<uint8_t>(interface::SBAND)] = false;
    m_rffe09.enable(false);
    m_rffe24.enable(false);
    m_pwr.enable(power::subsys::RF_5V, false);
  }
}

/**
 * @brief Enable/disable the radio chain of a specific band.
 *
 *
 * If the interface is already enabled, this method has no effect. If not,
 * it enables and set the default direction which is \ref rf_frontend::dir::RX
 *
 * @param iface the target interface (UHF/S-Band)
 * @param yes true to enable, false to disable
 */
void
radio::enable(interface iface, bool yes)
{
  int ret = 0;
  /* Enable the radio if it is not already enabled */
  if (yes && enabled() == false) {
    enable();
  }
  switch (iface) {
  case interface::UHF:
    if (yes) {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF09);
    } else {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF09);
    }
    ret += at86rf215_radio_irq_clear(&m_at86, AT86RF215_RF09);
    if (ret) {
      throw radio_exception(__FILE__, __LINE__);
    }
    m_rffe09.enable(yes);
    break;
  case interface::SBAND:
    if (yes) {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF24);
    } else {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF24);
    }
    ret += at86rf215_radio_irq_clear(&m_at86, AT86RF215_RF24);
    if (ret) {
      throw radio_exception(__FILE__, __LINE__);
    }
    m_rffe24.enable(yes);
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
  m_iface_enabled[static_cast<uint8_t>(iface)] = yes;
}

/**
 * @brief Checks if the radio transceiver is in an
 * operational state. This means that their powesr sources are enabled and each
 * component has been successfully initialized and there is basic connectivity
 * with the radio ICs
 *
 * @return true if all of the components of the radio chain are operational
 * @return false if at least one of the components of the radio is not
 * operational
 */
bool
radio::enabled() const
{
  return m_pwr.enabled(power::subsys::RF_5V) &&
         at86rf215_conn_check(&m_at86) == AT86RF215_OK;
}

/**
 * @brief Returns the state of a specific interface
 *
 * @param iface the desired interface
 * @return true if the interface is enabled
 * @return false if the interface is disabled
 *
 * @note If the radio subsystem is disabled, this method returns false
 */
bool
radio::enabled(interface iface) const
{
  return enabled() && m_iface_enabled[static_cast<uint8_t>(iface)];
}

/**
 * @brief Fetches the current direction (RX/TX) of the specific interface
 *
 * @param iface the desired interface
 * @return rf_frontend::dir the currently configured direction of the interface
 */
rf_frontend::dir
radio::direction(interface iface) const
{
  switch (iface) {
  case interface::UHF:
    return m_rffe09.direction();
    break;
  case interface::SBAND:
    return m_rffe24.direction();
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Sets the frequency and the direction
 *
 * @param iface the desired interface
 * @param d the direction to set
 * @param freq the frequency that will be applied after the direction change
 */
void
radio::set_frequency(interface iface, rf_frontend::dir d, float freq)
{
  int ret = 0;
  switch (iface) {
  case interface::UHF: {
    if (m_rffe09.frequency_valid(d, freq) == false) {
      throw unsupported_freq_exception(__FILE__, __LINE__);
    }

    m_rffe09.set_direction(d);
    struct at86rf215_radio_conf cnf = {.cm  = AT86RF215_CM_FINE_RES_04,
                                       .lbw = AT86RF215_PLL_LBW_DEFAULT};
    ret = at86rf215_radio_conf(&m_at86, AT86RF215_RF09, &cnf);
    if (ret) {
      throw radio_exception(__FILE__, __LINE__);
    }

    ret = at86rf215_set_freq(&m_at86, AT86RF215_RF09, freq);
    if (ret) {
      throw radio_exception(__FILE__, __LINE__);
    }
  } break;
  case interface::SBAND: {
    if (m_rffe24.frequency_valid(d, freq) == false) {
      throw unsupported_freq_exception(__FILE__, __LINE__);
    }

    float lo = lo_freq(freq, d);
    m_rffe24.set_direction(d, lo);
    struct at86rf215_radio_conf cnf = {.cm  = AT86RF215_CM_FINE_RES_24,
                                       .lbw = AT86RF215_PLL_LBW_DEFAULT};

    ret = at86rf215_radio_conf(&m_at86, AT86RF215_RF24, &cnf);
    if (ret) {
      throw radio_exception(__FILE__, __LINE__);
    }

    ret = at86rf215_set_freq(&m_at86, AT86RF215_RF24, if_freq(freq, d));
    if (ret) {
      throw radio_exception(__FILE__, __LINE__);
    }
  } break;
  }
}

/**
 * @brief Returns the actual frequency that the hardware reports
 *
 * @note For various reasons (tuning resolution, wrong configuration, etc) the
 * frequency requested may vary from the actual tuning frequency reported by the
 * hardware
 *
 * @bug This is not implemented yet and should return always 0.0
 *
 * @param iface the desired interface
 * @param d the direction to check the tuned frequency
 * @return float the tuned frequency as reported by the hardware
 */
float
radio::frequency(interface iface, rf_frontend::dir d) const
{
  // TODO
  return 0.0;
}

/**
 * @brief Sets the TX gain of the AT86RF215
 *
 * @param iface the desired interface
 * @param gain the target gain
 *
 * @note This method performs clamping on the given gain, ensuring that is
 * withing the [0, 31] range
 */
void
radio::set_tx_gain(interface iface, float gain)
{
  const at86rf215_radio_t r = iface_to_at86_radio(iface);
  gain                      = etl::clamp(gain, 0.0f, 31.0f);
  at86rf215_set_pac(&m_at86, r, AT86RF215_PACUR_NO_RED, std::round(gain));
}

/**
 * @brief Sets the RX gain settings for both the first and the second
 * (AT86RF215) stage.
 *
 * This method provides a single point of configuration for the RX gain stages.
 * Based on the #gain parameter, all possible configurations are supported.
 * Both stages can be set in a fixed gain mode or in AGC mode independently.
 * @see rf_frontend::rx_gain_params for more information
 *
 * @param iface the desired interface
 * @param gain the gain settings for the given interface.
 */
void
radio::set_rx_gain(interface iface, const rf_frontend::rx_gain_params &gain)
{
  const at86rf215_radio_t r = iface_to_at86_radio(iface);

  at86rf215_agc_conf agc_cnf = {.enable = 0,
                                .freeze = 0,
                                .reset  = 0,
                                .avgs   = AT86RF215_AVGS_8,
                                .input  = 0,
                                .gcw    = 23,
                                .tgt    = AT86RF215_TGT_M21};

  /* Manage gain settings for the AT86RF215 */
  if (gain.gain1_mode == rf_frontend::gain_mode::AUTO) {
    agc_cnf        = gain.gain1.agc;
    agc_cnf.enable = 1;
    agc_cnf.freeze = 0;
  } else {
    agc_cnf.enable = 0;
    agc_cnf.gcw =
        etl::clamp<uint8_t>(std::lroundf(gain.gain1.gain / 3.0f), 0U, 23U);
  }
  int ret = at86rf215_set_agc(&m_at86, r, &agc_cnf);
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }

  /* Now set the gain for the RF frontend */
  switch (iface) {
  case interface::UHF:
    m_rffe09.set_rx_gain(gain);
    break;
  case interface::SBAND:
    m_rffe24.set_rx_gain(gain);
    break;
  default:
    throw radio_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Checks if a given frequency is within the range of the mixer available
 * ranges
 *
 * @param p reference to a mixer parameter set
 * @param x the desired frequency
 * @return true if the given frequency is within the mixer params
 * @return false if it is not
 */
static bool
freq_in_range(const radio::mixer_param &p, float x)
{
  return x >= p.freq.first && x <= p.freq.second;
}

/**
 * @brief Gets the LO for the mixer based on a target RF frequency
 *
 * @param rf_freq the target RF frequency
 * @param d the direction that the RF frequency corresponds
 * @return float the LO frequency for the RF mixer
 *
 * @note If the frequency is not within a known frequency, the LO of the closest
 * frequency is used
 */
float
radio::lo_freq(float rf_freq, rf_frontend::dir d) const
{
  switch (d) {
  case rf_frontend::dir::RX: {

    for (const auto &i : m_rx_mixer_params) {
      if (freq_in_range(i, rf_freq)) {
        return i.lo;
      }
    }
    if (rf_freq < m_rx_mixer_params[0].freq.first) {
      return m_rx_mixer_params[0].lo;
    }
    return m_rx_mixer_params[1].lo;
  }
  case rf_frontend::dir::TX: {
    for (const auto &i : m_tx_mixer_params) {
      if (freq_in_range(i, rf_freq)) {
        return i.lo;
      }
    }
    if (rf_freq < m_tx_mixer_params[0].freq.first) {
      return m_tx_mixer_params[0].lo;
    }
    return m_tx_mixer_params[1].lo;
  }
  default:
    throw radio_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Converts the S-Band RF frequency to the equivalent IF, that should be
 * passed to the AT86RF215
 *
 * @note the conversion is based on the LO frequency (IF = RF + LO). @see
 * lo_freq() for more details
 *
 * @param rf_freq the RF frequency
 * @param d the direction that the RF frequency corresponds
 */
float
radio::if_freq(float rf_freq, rf_frontend::dir d) const
{
  return rf_freq + lo_freq(rf_freq, d);
}

void
radio::set_test_fsk(interface iface)
{
  const at86rf215_radio_t r = iface_to_at86_radio(iface);
  int ret = at86rf215_set_radio_irq_mask(&m_at86, r, TX_RF_IRQM);
  ret += at86rf215_set_bbc_irq_mask(&m_at86, r, TX_BB_IRQM);
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }

  /* FSK-2 50kHz */
  struct at86rf215_bb_conf at86_bb;
  at86_bb.ctx                    = 0;
  at86_bb.fcsfe                  = 0;
  at86_bb.txafcs                 = 1;
  at86_bb.fcst                   = AT86RF215_FCS_16;
  at86_bb.pt                     = AT86RF215_BB_MRFSK;
  at86_bb.fsk.mord               = AT86RF215_2FSK;
  at86_bb.fsk.midx               = AT86RF215_MIDX_3;
  at86_bb.fsk.midxs              = AT86RF215_MIDXS_88;
  at86_bb.fsk.bt                 = AT86RF215_FSK_BT_10;
  at86_bb.fsk.srate              = AT86RF215_FSK_SRATE_400;
  at86_bb.fsk.fi                 = 0;
  at86_bb.fsk.rxo                = AT86RF215_FSK_RXO_DISABLED;
  at86_bb.fsk.pdtm               = 0;
  at86_bb.fsk.rxpto              = 1;
  at86_bb.fsk.mse                = 0;
  at86_bb.fsk.pri                = 0;
  at86_bb.fsk.fecs               = AT86RF215_FSK_FEC_RSC;
  at86_bb.fsk.fecie              = 0;
  at86_bb.fsk.sfd_threshold      = 15;
  at86_bb.fsk.preamble_threshold = 8;
  at86_bb.fsk.sfdq               = 1;
  at86_bb.fsk.sfd32              = 1;
  at86_bb.fsk.rawrbit            = 1;
  at86_bb.fsk.csfd1              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.csfd0              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.preamble_length    = 64;
  /* SFD operates in a LSB order*/
  at86_bb.fsk.sfd0 = static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM));
  at86_bb.fsk.sfd1 =
      static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM) >> 16);
  at86_bb.fsk.sfd         = 0;
  at86_bb.fsk.dw          = 0;
  at86_bb.fsk.preemphasis = 0;
  at86_bb.fsk.dm          = 1;

  ret = at86rf215_bb_conf(&m_at86, r, &at86_bb);
  ret += at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
  ret += at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, r);
  ret += at86rf215_irq_clear(&m_at86);
  ret += at86rf215_bb_enable(&m_at86, r, 1);
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Performs a TX in blocking mode
 *
 * Performs a TX in blocking mode. If the radio was in RX state, it is
 * automatically set to TX mode. After the end of the transmission (or on
 * failure), the method leaves the radio in idle state.
 *
 * @note This method will perform internally the CCSDS encoding/scrambling and
 * it will append a CRC32-C checksum
 *
 * @warning This method requires that the corresponding frontend has been
 * enabled and configured to the desired frequency
 *
 * @param iface the interface to send the frame
 * @param b the frame buffer
 * @param len the lenfth of the frame buffer
 */
void
radio::tx(interface iface, const uint8_t *b, size_t len)
{

  const at86rf215_radio_t r = iface_to_at86_radio(iface);

  memcpy(tx_pdu, b, std::min(len, tx_len(iface)));
  auto crc =
      etl::crc32_c(tx_pdu, tx_pdu + tx_len(iface) - sizeof(uint32_t)).value();

  tx_pdu[tx_len(iface) - 4] = crc >> 24;
  tx_pdu[tx_len(iface) - 3] = crc >> 16;
  tx_pdu[tx_len(iface) - 2] = crc >> 8;
  tx_pdu[tx_len(iface) - 1] = crc;

  dsp::ccsds::scrambler(tx_pdu, tx_len(iface));

  int ret = at86rf215_tx_frame(&m_at86, r, tx_pdu, tx_len(iface), 4000);
  if (ret) {
    incr_tx_fail_frames(iface);
    throw radio_exception(__FILE__, __LINE__);
  }
  incr_tx_frames(iface);
}

/**
 * @brief Finds the best combination for the FSK modulation index and its
 * scaling factor for the AT86RF215 based on a user defined modulation index
 *
 * @param mod_idx the desired modulation index
 *
 * @return std::pair<at86rf215_fsk_midx_t, at86rf215_fsk_midxs_t> with the first
 * element to contain the best modulation index and the second one the scaling
 * factor
 */
static std::pair<at86rf215_fsk_midx_t, at86rf215_fsk_midxs_t>
tune_fsk_mod_idx(float mod_idx)
{
  float   diff = std::fabs(mod_idx - fsk_mod_idxs[0] * 7.0f / 8.0f);
  uint8_t mod  = 0;
  uint8_t s    = 0;
  for (uint8_t i = 0; i < fsk_mod_idxs.size(); i++) {
    float scaling = 7.0f / 8.0f;
    for (uint8_t j = 0; j < 4; j++) {
      const auto x = std::fabs(mod_idx - fsk_mod_idxs[i] * scaling);
      if (x < diff) {
        mod  = i;
        s    = j;
        diff = x;
      }
      scaling += 1.0f / 8.0f;
    }
  }
  return {static_cast<at86rf215_fsk_midx_t>(mod),
          static_cast<at86rf215_fsk_midxs_t>(s)};
}

/**
 * @brief Finds the best shaping filter bandwidth based on a user defined value
 *
 * @param bt the user defined desired shaping filter 3-dB normalized bandwidth
 * @return at86rf215_fsk_bt_t
 */
static at86rf215_fsk_bt_t
tune_fsk_bt(float bt)
{
  float   diff = std::fabs(bt - fsk_bt[0]);
  uint8_t idx  = 0;
  for (uint8_t i = 1; i < fsk_bt.size(); i++) {
    const float x = std::fabs(bt - fsk_bt[i]);
    if (x < diff) {
      diff = x;
      idx  = i;
    }
  }
  return static_cast<at86rf215_fsk_bt_t>(idx);
}

/**
 * @brief Sets the radio in reception mode asynchronously
 *
 * This method sets the radio in RX state with the given configuration
 * parameters and exits immediately. Any received frame will be pushed on the
 * reception message queue of the radio subsystem. Users can poll for received
 * messages utilizing the radio::recv_msg() method
 * @param iface the desired interface
 * @param conf the reception configuration for the RF frontend and the modem
 */
void
radio::rx_async(interface iface, const rx_conf &conf)
{
  enable(iface);
  set_frequency(iface, rf_frontend::dir::RX, conf.freq);
  const at86rf215_radio_t r = iface_to_at86_radio(iface);

  int ret = at86rf215_set_radio_irq_mask(&m_at86, r, RX_RF_IRQM);
  ret += at86rf215_set_bbc_irq_mask(&m_at86, r, RX_BB_IRQM);
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }

  set_rx_gain(iface, conf.gain);

  /* Fetch the best settings that match the user defined modulation index */
  auto mod_idx = tune_fsk_mod_idx(conf.fsk.mod_idx);

  /* FSK-2 50kHz */
  struct at86rf215_bb_conf at86_bb;
  at86_bb.ctx                    = 0;
  at86_bb.fcsfe                  = 0;
  at86_bb.txafcs                 = 1;
  at86_bb.fcst                   = AT86RF215_FCS_16;
  at86_bb.pt                     = AT86RF215_BB_MRFSK;
  at86_bb.fsk.mord               = AT86RF215_2FSK;
  at86_bb.fsk.midx               = mod_idx.first;
  at86_bb.fsk.midxs              = mod_idx.second;
  at86_bb.fsk.bt                 = tune_fsk_bt(conf.fsk.excess_bw);
  at86_bb.fsk.srate              = conf.fsk.rate;
  at86_bb.fsk.fi                 = 0;
  at86_bb.fsk.rxo                = AT86RF215_FSK_RXO_DISABLED;
  at86_bb.fsk.pdtm               = 0;
  at86_bb.fsk.rxpto              = 1;
  at86_bb.fsk.mse                = 0;
  at86_bb.fsk.pri                = 0;
  at86_bb.fsk.fecs               = AT86RF215_FSK_FEC_RSC;
  at86_bb.fsk.fecie              = 0;
  at86_bb.fsk.sfd_threshold      = 12;
  at86_bb.fsk.preamble_threshold = 4;
  at86_bb.fsk.sfdq               = 0;
  at86_bb.fsk.sfd32              = 1;
  at86_bb.fsk.rawrbit            = 1; // Most of the encoders transmit MSB first
  at86_bb.fsk.csfd1              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.csfd0              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.preamble_length    = 16;
  /* SFD operates in a LSB order*/
  at86_bb.fsk.sfd0 = static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM));
  at86_bb.fsk.sfd1 =
      static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM) >> 16);
  at86_bb.fsk.sfd         = 0;
  at86_bb.fsk.dw          = 0;
  at86_bb.fsk.preemphasis = 0;
  at86_bb.fsk.dm          = 1;

  ret = at86rf215_bb_conf(&m_at86, r, &at86_bb);
  ret += at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
  ret += at86rf215_irq_clear(&m_at86);
  ret += at86rf215_bb_enable(&m_at86, r, 1);
  ret += at86rf215_rx(&m_at86, r, 1000);
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Gets the status of the RF mixer lock
 *
 * @return true if the mixer is locked
 * @return false if the mixer is not locked or does not communicate
 */
bool
radio::mixer_lock()
{
  return m_rffe24.mixer_lock();
}

/**
 *
 * @return rf_frontend09& of the UHF RF front-end
 */
rf_frontend09 &
radio::uhf()
{
  return m_rffe09;
}

/**
 *
 * @return rf_frontend24& of the S-band RF front-end
 */
rf_frontend24 &
radio::sband()
{
  return m_rffe24;
}

/**
 * @brief Gets an available message from the receive message queue
 *
 * @param msg reference to place the received message
 * @param timeout_ms the timeout in milliseconds
 * @return 0 on success or negative error code on failure or timeout
 */
int
radio::recv_msg(rx_msg &msg, size_t timeout_ms)
{
  int ret = m_rxq.get(&msg, timeout_ms);
  if (ret) {
    return ret;
  }

  /* Check the CRC */
  auto crc =
      etl::crc32_c(msg.pdu, msg.pdu + msg.info.len - sizeof(uint32_t)).value();
  uint32_t recv = (msg.pdu[msg.info.len - sizeof(uint32_t)] << 24) |
                  (msg.pdu[msg.info.len - sizeof(uint32_t) + 1] << 16) |
                  (msg.pdu[msg.info.len - sizeof(uint32_t) + 2] << 8) |
                  msg.pdu[msg.info.len - sizeof(uint32_t) + 3];
  if (crc == recv) {
    incr_rx_frames(msg.info.iface);
    return 0;
  }

  incr_rx_inval_frames(msg.info.iface);
  // FIXME Define error codes?
  return -1;
}

/**
 * @brief Gets the statistics for a specific interface
 *
 * @param iface the desired interface
 * @return const radio::stats& holding the statistics of the corresponding
 * interface
 */
const radio::stats &
radio::get_stats(interface iface) const
{
  return m_stats[iface_idx(iface)];
}

/**
 * @brief Retrieves the configured TX frame size for a specific interface
 *
 * @param iface the desired interface
 * @return uint32_t the TX frame size of the specified interface
 */
uint32_t
radio::tx_len(interface iface) const
{
  return m_frame_len[iface_idx(iface)].tx;
}

/**
 * @brief Retrieves the configured RX frame size for a specific interface
 *
 * @param iface the desired interface
 * @return uint32_t the RX frame size of the specified interface
 */
uint32_t
radio::rx_len(interface iface) const
{
  return m_frame_len[iface_idx(iface)].rx;
}

/**
 * Internal structures initialization.
 *
 * @warning Touch it only if you know what you are doing!
 *
 */
void
radio::bsp_setup()
{
  m_at86.spi_dev      = &m_spi;
  m_at86.cs_gpio_dev  = &m_spi.cs();
  m_at86.rst_gpio_dev = &m_nrst;
  /* We use the user0 field for the chrono related stuff */
  m_at86.user_dev0 = &m_chrono;
  /*
   * user1 has the class itself for handling the user defined IRQ handler of
   * the AT86RF215 driver
   */
  m_at86.user_dev1 = this;
}

/**
 * @brief Enable/disable the IC
 *
 * @param enable true to enable, false otherwise
 */
void
radio::at86rf215_enable(bool enable)
{
  /* In case the user needs to disable the IC, just de-assert the RSTN pin */
  if (enable == false) {
    at86rf215_set_rstn(&m_at86, 0);
    return;
  }

  m_at86.clk_drv      = AT86RF215_RF_DRVCLKO4;
  m_at86.clko_os      = AT86RF215_RF_CLKO_26_MHZ;
  m_at86.xo_trim      = 0;
  m_at86.xo_fs        = 0;
  m_at86.irqp         = 0;
  m_at86.irqmm        = 0;
  m_at86.pad_drv      = AT86RF215_RF_DRV8;
  m_at86.rf_femode_09 = AT86RF215_RF_FEMODE2;
  m_at86.rf_femode_24 = AT86RF215_RF_FEMODE2;

  int ret = at86rf215_init(&m_at86);
  /*
   * init() makes several checks so it will be good to raise an exception
   * specifically for it
   */
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }

  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF09);
  ret += at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF24);
  if (ret) {
    throw radio_exception(__FILE__, __LINE__);
  }
}

void
radio::incr_tx_frames(interface iface)
{
  if (m_stats[iface_idx(iface)].tx_frames <
      std::numeric_limits<size_t>::max()) {
    m_stats[iface_idx(iface)].tx_frames++;
  }
}

void
radio::incr_tx_fail_frames(interface iface)
{
  if (m_stats[iface_idx(iface)].tx_frames_fail <
      std::numeric_limits<size_t>::max()) {
    m_stats[iface_idx(iface)].tx_frames_fail++;
  }
}

/**
 * @brief Resets the radio statistics of an interface
 *
 * @param iface the desired interface
 */
void
radio::reset_stats(interface iface)
{
  m_stats[iface_idx(iface)].tx_frames       = 0;
  m_stats[iface_idx(iface)].tx_frames_fail  = 0;
  m_stats[iface_idx(iface)].tx_frames_drop  = 0;
  m_stats[iface_idx(iface)].rx_frames       = 0;
  m_stats[iface_idx(iface)].rx_frames_inval = 0;
  m_stats[iface_idx(iface)].rx_frames_drop  = 0;
}

void
radio::incr_tx_drop_frames(interface iface)
{
  if (m_stats[iface_idx(iface)].tx_frames_drop <
      std::numeric_limits<size_t>::max()) {
    m_stats[iface_idx(iface)].tx_frames_drop++;
  }
}

void
radio::incr_rx_frames(interface iface)
{
  if (m_stats[iface_idx(iface)].rx_frames <
      std::numeric_limits<size_t>::max()) {
    m_stats[iface_idx(iface)].rx_frames++;
  }
}

void
radio::incr_rx_inval_frames(interface iface)
{
  if (m_stats[iface_idx(iface)].rx_frames_inval <
      std::numeric_limits<size_t>::max()) {
    m_stats[iface_idx(iface)].rx_frames_inval++;
  }
}

void
radio::incr_rx_drop_frames(interface iface)
{
  if (m_stats[iface_idx(iface)].rx_frames_drop <
      std::numeric_limits<size_t>::max()) {
    m_stats[iface_idx(iface)].rx_frames_drop++;
  }
}

uint32_t
radio::iface_idx(interface iface) const
{
  switch (iface) {
  case interface::UHF:
  case interface::SBAND:
    return static_cast<uint32_t>(iface);
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

} // namespace satnogs::comms
