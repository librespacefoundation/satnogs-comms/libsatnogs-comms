/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/leds.hpp>

namespace satnogs::comms
{
/**
 * @brief Constructs an instance of the @ref leds class.
 *
 * @param led0 Reference to the @ref bsp::gpio object controlling the LED_0 of
 * the the SatNOGS-COMMS board
 * @param led1 Reference to the @ref bsp::gpio object controlling LED_1 of the
 * the SatNOGS-COMMS board
 */
leds::leds(bsp::gpio &led0, bsp::gpio &led1)
    : m_leds({{led::led0, led0}, {led::led1, led1}})
{
}

/**
 * @brief Toggles the state of the specified LED
 *
 * Change the state of a specific @ref led to the opposite of its current state
 *
 * @param x The LED to toggle.
 */
void
leds::toggle(led x)
{
  m_leds.at(x).toggle();
}

/**
 * @brief Enables or disables the specified @ref led.
 *
 * @param x The @ref led to modify.
 * @param en Passing `true` will set the @ref led will set it to its active
 * state, which can be high or low, based on the @ref bsp::gpio object. Passing
 * `false` will cause the opposite effect configuration. Default parameter is
 * `true`.
 */
void
leds::enable(led x, bool en)
{
  m_leds.at(x).set(en);
}

/**
 * @brief Disables the specified @ref led.
 *
 * This is a shorthand for calling `enable(x, false)` to explicitly disable a
 * @ref led
 *
 * @param x The @ref led to disable.
 */
void
leds::disable(led x)
{
  enable(x, false);
}

/**
 * @brief Checks if the specified @ref led is currently enabled.
 *
 * @param x The @ref led to check.
 * @return `true` if the @ref led is set to its active state, `false` otherwise.
 */
bool
leds::enabled(led x) const
{
  return m_leds.at(x).get();
}

} // namespace satnogs::comms
