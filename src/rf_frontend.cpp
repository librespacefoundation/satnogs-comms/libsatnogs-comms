/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/board.hpp>
#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

rf_frontend::rf_frontend(const params &init_params, io_conf &&io, power &pwr)
    : m_params(init_params),
      m_filt_sel(io.flt_sel),
      m_dac(io.agc_vset),
      m_pwr(pwr),
      m_agc(io.en_agc),
      m_dir(dir::RX)
{
  /* By default select the narrow filter */
  set_filter(filter::NARROW);
}

/**
 * Sets the RX hardware filter path
 * @param f the RF filter path (filter::NARROW or filter::WIDE)
 */
void
rf_frontend::set_filter(filter f)
{
  switch (f) {
  case filter::WIDE:
    m_filt_sel.set(false);
    break;
  case filter::NARROW:
    m_filt_sel.set(true);
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 *
 * @return the selected RX hardware filter path
 */
rf_frontend::filter
rf_frontend::get_filter() const
{
  if (m_filt_sel.get()) {
    return filter::NARROW;
  }
  return filter::WIDE;
}

/**
 *
 * @return rf_frontend::dir the direction that the RF-frontend currently
 * operates
 */
rf_frontend::dir
rf_frontend::direction() const
{
  return m_dir;
}

/**
 * @brief Sets the RX gain parameters
 *
 * @param gain the gain parameters. Only the first stage gain is used by the
 * rf_frontend class
 */
void
rf_frontend::set_rx_gain(const rx_gain_params &gain)
{
  switch (gain.gain0_mode) {
  case gain_mode::AUTO: {
    m_agc.enable(true);
    m_dac.start();
    float x = etl::clamp(gain.gain0.tgt, m_params.agc0_range.first,
                         m_params.agc0_range.second);
    m_dac.set_voltage(m_params.agc0_calib.first * x +
                      m_params.agc0_calib.second);
  } break;
  case gain_mode::MANUAL: {
    m_agc.enable(false);
    m_dac.start();
    float x = etl::clamp(gain.gain0.gain, m_params.gain0_range.first,
                         m_params.gain0_range.second);
    m_dac.set_voltage(m_params.gain0_calib.first * x +
                      m_params.gain0_calib.second);
  } break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Checks if a frequency is within the permissible range
 *
 * @param d the direction (RX or TX)
 * @param freq the desired frequency
 * @return true if the frequency is within the Permissible range, false
 * otherwise
 */
bool
rf_frontend::frequency_valid(dir d, float freq) const
{
  switch (d) {
  case dir::RX:
    if (freq < m_params.rx_range.first || freq > m_params.rx_range.second) {
      return false;
    }
    return true;
  case dir::TX:
    if (freq < m_params.tx_range.first || freq > m_params.tx_range.second) {
      return false;
    }
    return true;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

} // namespace satnogs::comms
