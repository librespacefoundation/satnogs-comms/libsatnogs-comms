/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/board.hpp>
#include <satnogs-comms/emmc.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

/**
 * Creates a generic EMMC handler
 * @param en GPIO to enable/disable the EMMC
 * @param sel GPIO to select which of MCU or FPGA has access to the EMMC
 * @param rst GPIO for the reset signal of the EMMC
 */
emmc::emmc(bsp::gpio &en, bsp::gpio &sel, bsp::gpio &rst)
    : m_en_gpio(en), m_sel_gpio(sel), m_rst_gpio(rst)
{
  enable(false);
}

/**
 * Enable/disable the EMMC
 * @param set set to true to enable the EMMC, false to disable.
 */
void
emmc::enable(bool set)
{
  m_en_gpio.set(set);
}

/**
 * Reset the EMMC
 * @param set set to true to enable the EMMC reset pin, false to disable it.
 */
void
emmc::reset(bool set)
{
  m_rst_gpio.set(set);
}

/**
 *
 * @return true of the EMMC is enabled, false otherwise
 */
bool
emmc::enabled() const
{
  return m_en_gpio.get();
}

/**
 * Sets the direction of the EMMC
 * @param d the direction
 */
void
emmc::set_dir(dir d)
{

  switch (d) {
  case dir::MCU:
    m_sel_gpio.set(true);
    break;
  case dir::FPGA:
    m_sel_gpio.set(false);
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 *
 * @return the direction of the EMMC
 */
emmc::dir
emmc::get_dir() const
{
  if (m_sel_gpio.get() == true) {
    return dir::MCU;
  }
  return dir::FPGA;
}

} // namespace satnogs::comms
