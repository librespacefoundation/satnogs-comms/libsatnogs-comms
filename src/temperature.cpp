#include <satnogs-comms/board.hpp>
#include <satnogs-comms/temperature.hpp>

namespace satnogs::comms
{

template <>
float
temperature<emc1702>::get(temperature_sensor s) const
{
  switch (s) {
  case temperature_sensor::PCB:
    return m_pcb.get_temperature_average();
  case temperature_sensor::UHF_PA:
    return m_uhf.get_temperature_average();
  case temperature_sensor::SBAND_PA:
    return m_sband.get_temperature_average();
  default:
    throw invalid_sensor_exception(__FILE__, __LINE__);
  }
}

template <>
bool
temperature<emc1702>::alert(temperature_sensor s) const
{
  switch (s) {
  case temperature_sensor::PCB:
    return m_pcb.alert();
  case temperature_sensor::UHF_PA:
    return m_uhf.alert();
  case temperature_sensor::SBAND_PA:
    return m_sband.alert();
  default:
    throw invalid_sensor_exception(__FILE__, __LINE__);
    return false;
  }
}

} // namespace satnogs::comms
