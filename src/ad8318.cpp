/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

ad8318::ad8318(bsp::gpio &en) : m_en_gpio(en) { enable(false); }

/**
 * Enables/Disables the AD8318
 * @param set set to true to enable or false to disable
 */
void
ad8318::enable(bool set)
{
  m_en_gpio.set(set);
}

/**
 * Toggles the state of the AD8318
 */
void
ad8318::toggle()
{
  enable(!enabled());
}

/**
 *
 * @return true if the IC is enabled, false otherwise
 */
bool
ad8318::enabled() const
{
  return m_en_gpio.get();
}

} // namespace satnogs::comms
