/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/board.hpp>

namespace satnogs::comms
{
static board *m_instance = nullptr;

/**
 * @brief Initializes the board and creates a single instance
 *
 * @warning This static method should be called only once
 * @param io IO configuration
 * @param p initialization parameters
 */
void
board::init(const io_conf &io, const params &p)
{
  if ((m_instance != nullptr) && m_instance->m_init) {
    throw initialization_exception(__FILE__, __LINE__);
  }
  static board inst(io, p);
  m_instance         = &inst;
  m_instance->m_init = true;
}

/**
 * @brief Checks if the init() has already called by the user
 *
 * @return true if init() has already called and the single instance for the
 * \ref board class has been defined
 * @return false if the init() has not yet called
 */
bool
board::is_init()
{
  return (m_instance != nullptr) && m_instance->m_init;
}

/**
 * @brief Gets a reference to the single instance of the \ref board class
 * @warning Call init() first, otherwise a uninitialization_exception will be
 * thrown
 * @return board&
 */
board &
board::get_instance()
{
  if ((m_instance == nullptr) || !m_instance->m_init) {
    throw uninitialization_exception(__FILE__, __LINE__);
  }
  return *m_instance;
}

/**
 * @brief Returns a reference to the radio subsystem
 *
 * @return comms::radio&
 */
comms::radio &
board::radio()
{
  return m_radio;
}

/**
 * @brief Returns a reference to the eMMC subsystem
 *
 * @return comms::emmc&
 */
comms::emmc &
board::emmc()
{
  return m_emmc;
}

/**
 * @brief Returns a reference to the power subsystem
 *
 * @return comms::power&
 */
comms::power &
board::power()
{
  return m_power;
}

/**
 * @brief Returns a reference to the FPGA subsystem
 *
 * @return comms::fpga&
 */
comms::fpga &
board::fpga()
{
  return m_fpga;
}

/**
 * @brief Returns a reference to the LEDs subsystem
 *
 * @return comms::leds&
 */
comms::leds &
board::leds()
{
  return m_leds;
}

antenna &
board::antenna(radio::interface iface)
{
  switch (iface) {
  case radio::interface::UHF:
    return m_uhf_antenna;
  case radio::interface::SBAND:
    return m_sband_antenna;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Checks if there is an alert from a specific sensor
 *
 * @param s the target sensor
 * @return true if the alert is set
 * @return false if there is no alert
 */
bool
board::alert(temperature_sensor s) const
{
  return m_temperature.alert(s);
}

/**
 * @brief Gets the temperature from a specific sensor
 *
 * @param s the target sensor
 * @return float with the temperature in Celsius
 */
float
board::temperature(temperature_sensor s) const
{
  return m_temperature.get(s);
}

board::board(const io_conf &io, const params &p)
    : m_init(false),
      m_power(io.pwr_io),
      m_radio(p.radio_params, io.radio_io, m_power, p.rx_msgq),
      m_fpga(p.fpga_hw, io.fpga_spi, m_power, io.fpga_done),
      m_emmc(io.emmc_en, io.emmc_sel, io.emmc_rst),
      m_uhf_antenna(io.uhf_antenna),
      m_sband_antenna(io.sband_antenna),
      m_leds(io.led0, io.led1),
      m_temperature(io.sensors_i2c, io.alert_t_pa_uhf, io.alert_t_pa_sband,
                    m_radio.uhf(), m_radio.sband())
{
}

} // namespace satnogs::comms
