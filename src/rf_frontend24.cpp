/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/board.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/rf_frontend24.hpp>

namespace satnogs::comms
{

/* Re-implementation of the RFFC2071 driver weak functions */
extern "C" {

int
rffcx07x_set_enx(struct rffcx07x *h, uint8_t enable)
{
  if (h->enx_gpio_dev) {
    bsp::gpio *enx_gpio_dev = static_cast<bsp::gpio *>(h->enx_gpio_dev);
    enx_gpio_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_enbl(struct rffcx07x *h, uint8_t enable)
{
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_resetx(struct rffcx07x *h, uint8_t enable)
{
  if (h->rst_gpio_dev) {
    bsp::gpio *rst_gpio_dev = static_cast<bsp::gpio *>(h->rst_gpio_dev);
    rst_gpio_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_mode(struct rffcx07x *h, uint8_t enable)
{
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_clk(struct rffcx07x *h, uint8_t enable)
{
  if (h->clk_dev) {
    bsp::gpio *clk_dev = static_cast<bsp::gpio *>(h->clk_dev);
    clk_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_sda(struct rffcx07x *h, uint8_t enable)
{
  if (h->sda_gpio_dev) {
    bsp::gpio *sda_gpio_dev = static_cast<bsp::gpio *>(h->sda_gpio_dev);
    sda_gpio_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_get_sda(struct rffcx07x *h)
{
  if (h->sda_gpio_dev) {
    bsp::gpio *sda_gpio_dev = static_cast<bsp::gpio *>(h->sda_gpio_dev);
    return sda_gpio_dev->get();
  }
  return -RFFCX07X_NOT_IMPL;
}

int
rffcx07x_set_sda_dir(struct rffcx07x *h, rffcx07x_sda_dir_t dir)
{
  if (h->sda_gpio_dev) {
    bsp::gpio *sda_gpio_dev = static_cast<bsp::gpio *>(h->sda_gpio_dev);
    if (dir == RFFCX07X_SDA_RX) {
      sda_gpio_dev->set_direction(bsp::gpio::direction::INPUT);
    } else {
      sda_gpio_dev->set_direction(bsp::gpio::direction::OUTPUT);
    }
  }
  return RFFCX07X_NO_ERROR;
}

void
rffcx07x_en_irq(struct rffcx07x *h, uint8_t enable)
{
  return;
}

void
rffcx07x_delay_us(struct rffcx07x *h, uint32_t us)
{
  bsp::chrono *chrono_dev = static_cast<bsp::chrono *>(h->user_dev0);
  chrono_dev->delay_us(us);
}
}

/**
 * @brief Construct a new rf frontend24::rf frontend24 object that controls the
 * S-Band RF frontend interface
 *
 * @param init_params the initialization parameters
 * @param io the IO configuration for controlling the RF mixer and the
 * RF-frontend first stage RX gain as well as the filter selection circuit
 * @param pwr reference to power subsystem, used to power up/down the various
 * subsystems of the S-Band RF frontend
 */
rf_frontend24::rf_frontend24(const params &init_params, const io_conf &io,
                             power &pwr)
    : rf_frontend(init_params, {io.en_agc, io.agc_vset, io.flt_sel}, pwr),
      m_chrono(io.chrono)
{
  m_mixer.clk_dev      = &io.mixer_clk;
  m_mixer.rst_gpio_dev = &io.mixer_rst;
  m_mixer.enx_gpio_dev = &io.mixer_enx;
  m_mixer.sda_gpio_dev = &io.mixer_sda;
  m_mixer.user_dev0    = &io.chrono;
  enable(false);
}

/**
 * Enables the basic subsystems of the RF frontend. Subsystems that are
 * used for a specific direction (RX or TX) are explicitly disabled to reduce
 * power consumption.
 *
 * Users may have to call the set_direction() method to enable the corresponding
 * subsystems such as PA or LNA.
 *
 * @param set true to enable, false to disable.
 */
void
rf_frontend24::enable(bool set)
{
  int ret = 0;
  m_pwr.enable(power::subsys::SBAND, set);
  if (set) {
    m_chrono.delay_us(2000);
    ret = rffcx07x_init(&m_mixer, 26000000, 72800, RFFCX07X_HD);
    ret += rffcx07x_set_gate(&m_mixer, 1);
    ret += rffcx07x_set_lock_gpo(&m_mixer, 1);
    if (ret) {
      throw rf_frontend24_exception(__FILE__, __LINE__);
    }
  } else {
    /* Explicitly set the ENBL and RESETX to 0, because the mixer can
     * parasitically still  operate */
    rffcx07x_set_enx(&m_mixer, 0);
    rffcx07x_set_resetx(&m_mixer, 0);
  }
}

/**
 * @brief Checks if the S-Band RF frontend is enabled
 *
 * @return true if it is enabled
 * @return false  if it is disabled
 */
bool
rf_frontend24::enabled() const
{
  return m_pwr.enabled(power::subsys::SBAND);
}

/**
 * Sets the direction of the RF frontend, enables the corresponding components
 * and disables the components that are not needed to reduce consumption
 * @param d the direction
 * @param lo_freq the frequency of the LO
 */
void
rf_frontend24::set_direction(dir d, float lo_freq)
{
  int ret = 0;
  switch (d) {
  case dir::RX:
    m_dir = d;
    ret += rffcx07x_enable(&m_mixer, 0, RFFCX07X_PATH2, 1);
    ret += rffcx07x_set_mix_current(&m_mixer, 2, RFFCX07X_PATH1);
    ret += rffcx07x_set_freq(&m_mixer, lo_freq, RFFCX07X_PATH1);
    ret += rffcx07x_enable(&m_mixer, 1, RFFCX07X_PATH1, 1);
    if (ret) {
      throw rf_frontend24_exception(__FILE__, __LINE__);
    }

    if (mixer_lock() == false) {
      throw mixer_lock_exception(__FILE__, __LINE__);
    }
    break;
  case dir::TX:
    m_dir = d;
    m_agc.enable(false);
    m_dac.stop();
    ret += rffcx07x_enable(&m_mixer, 0, RFFCX07X_PATH1, 1);
    ret = rffcx07x_set_mix_current(&m_mixer, 2, RFFCX07X_PATH2);
    ret += rffcx07x_set_freq(&m_mixer, lo_freq, RFFCX07X_PATH2);
    ret += rffcx07x_enable(&m_mixer, 1, RFFCX07X_PATH2, 1);
    if (ret) {
      throw rf_frontend24_exception(__FILE__, __LINE__);
    }

    if (mixer_lock() == false) {
      throw mixer_lock_exception(__FILE__, __LINE__);
    }
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
    break;
  }
}

/**
 * @brief Checks if the RF mixer has acquired a lock.
 *
 * @note The RF mixer locks if all of the following requirements are met:
 * -# the reference clock source is enabled
 * -# the configured frequency matches the reference clock
 * -# Valid LO frequencies on either two available paths of the mixer have been
 * configured
 *
 * @return true if the RF mixer has acquired a lock
 * @return false if the RF mixer has not acquired a lock
 */
bool
rf_frontend24::mixer_lock()
{
  uint8_t lock = false;
  int     ret  = rffcx07x_get_lock(&m_mixer, &lock);
  return lock && ret == RFFCX07X_NO_ERROR;
}

} // namespace satnogs::comms
