# ##############################################################################
# SatNOGS-COMMS control library
#
# Copyright (C) 2022, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GNU General Public License v3.0 or later
# ##############################################################################

add_library(libsatnogs-comms-drivers INTERFACE)

target_sources(libsatnogs-comms-drivers INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/at86rf215-driver/src/at86rf215.c
    ${CMAKE_CURRENT_LIST_DIR}/rffcx07x-driver/src/rffcx07x.c
)

target_include_directories(libsatnogs-comms-drivers INTERFACE
    ${CMAKE_CURRENT_LIST_DIR}/at86rf215-driver/include
    ${CMAKE_CURRENT_LIST_DIR}/rffcx07x-driver/include
)
