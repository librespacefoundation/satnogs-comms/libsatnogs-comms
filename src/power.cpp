/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/board.hpp>
#include <satnogs-comms/power.hpp>

namespace satnogs::comms
{

/**
 * @brief Enable/disable the power of subsystems
 *
 * @param sys the target subsystem/power supply
 * @param en true to enable, false to disable
 */
void
power::enable(subsys sys, bool en)
{
  switch (sys) {
  case subsys::CAN1:
    m_can1_en.set(en);
    m_can1_low_pwr.set(!en);
    break;
  case subsys::CAN2:
    m_can2_en.set(en);
    m_can2_low_pwr.set(!en);
    break;
  case subsys::RF_5V:
    if (en) {
      m_imon_5v_rf.start();
    } else {
      m_imon_5v_rf.stop();
    }
    m_rf_5v_en.set(en);
    break;
  case subsys::FPGA_5V:
    if (en) {
      m_imon_fpga.start();
    } else {
      m_imon_fpga.stop();
    }
    m_fpga_5v_en.set(en);
    break;
  case subsys::CAN1_LPWR:
    m_can1_low_pwr.set(en);
    break;
  case subsys::CAN2_LPWR:
    m_can2_low_pwr.set(en);
    break;
  case subsys::UHF:
    m_uhf_en.set(en);
    break;
  case subsys::SBAND:
    m_sband_en.set(en);
    break;
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Checks the if the subsystem is enabled
 *
 * @param sys the subsystem to check
 * @return true if the subsystem is enabled, false otherwise
 */
bool
power::enabled(subsys sys) const
{
  switch (sys) {
  case subsys::CAN1:
    return m_can1_en.get();
  case subsys::CAN2:
    return m_can2_en.get();
  case subsys::RF_5V:
    return m_rf_5v_en.get();
  case subsys::FPGA_5V:
    return m_fpga_5v_en.get();
  case subsys::CAN1_LPWR:
    return m_can1_low_pwr.get();
  case subsys::CAN2_LPWR:
    return m_can2_low_pwr.get();
  case subsys::UHF:
    return m_uhf_en.get();
  case subsys::SBAND:
    return m_sband_en.get();
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Gets the power good indication from various power supplies
 *
 * @param tp the power supply
 * @return true if the power is good and stabilized, false otherwise
 */
bool
power::pgood(pgood_tp tp) const
{
  switch (tp) {
  /* In v0.3 if the 3V3 has fault, the MCU will reset */
  case pgood_tp::RAIL_5V:
    return m_pgood_5v.get();
  case pgood_tp::RAIL_FPGA:
    return m_pgood_fpga.get();
  case pgood_tp::RAIL_UHF:
    return m_uhf_pgood.get();
  case pgood_tp::RAIL_SBAND:
    return m_sband_pgood.get();
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Retrieves the voltage of a specified channel.
 *
 * This function measures the voltage for a specific power channel in the
 * system. Supported channels include the input voltage (`VIN`) and battery
 * voltage (`V_BAT`).
 *
 * @param sys The channel for which the voltage is being measured
 * @return The measured voltage in Volts.
 * @throws `inval_arg_exception` if the specified channel is not supported.
 */
float
power::voltage(channel sys) const
{
  switch (sys) {
  case channel::VIN:
    return m_monitor.get_source_voltage();
  case channel::V_BAT:
    return m_v_mon.vbat();
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

/**
 * @brief Retrieves the current of a specified channel.
 *
 * This function measures the current for a specific power channel using the
 * corresponding ADC and resistor limits. Supported channels include the digital
 * 3.3V rail, RF 5V rail, FPGA rail, and input voltage channel.
 *
 * @param sys The channel for which the current is being measured:
 * @return The measured current in amperes (A).
 * @throws inval_arg_exception if the specified channel is not supported.
 */
float
power::current(channel sys) const
{
  switch (sys) {
  case channel::DIG_3V3:
    return (m_imon_3v3.voltage() * 1000000 /
            (m_efuse_adc_current_gain * m_r_lim.dig_3v3));
  case channel::RF_5V:
    return (m_imon_5v_rf.voltage() * 1000000 /
            (m_efuse_adc_current_gain * m_r_lim.rf_5v));
  case channel::FPGA:
    return (m_imon_fpga.voltage() * 1000000 /
            (m_efuse_adc_current_gain * m_r_lim.fpga_5v));
  case channel::VIN:
    return m_monitor.get_sense_current();
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @brief Calculates the power consumption for a specified sensor source.
 *
 * This function computes the power consumption based on the specified power
 * sensor source. It supports efuses, EMC1702 sensor, and an averaged value.
 *
 * @param src The power sensor source
 * @return The calculated power in Watts.
 * @throws inval_arg_exception if the specified sensor source is not supported.
 */
float
power::get_power(sensor src) const
{
  switch (src) {
  case sensor::EFUSES:
    return (power::current(channel::DIG_3V3) * 3.3f +
            power::current(channel::RF_5V) * 5.0f +
            power::current(channel::FPGA) * 5.0f);
  case sensor::EMC1702:
    return m_monitor.get_power();
  case sensor::AVERAGE:
    return (((power::current(channel::DIG_3V3) * 3.3f +
              power::current(channel::RF_5V) * 5.0f +
              power::current(channel::FPGA) * 5.0f) +
             m_monitor.get_power()) /
            2);
  default:
    throw inval_arg_exception(__FILE__, __LINE__);
  }
}

power::power(const io_conf &io)
    : m_rf_5v_en(io.rf_5v_en),
      m_fpga_5v_en(io.fpga_5v_en),
      m_can1_en(io.can1_en),
      m_can1_low_pwr(io.can1_low_pwr),
      m_can2_en(io.can2_en),
      m_can2_low_pwr(io.can2_low_pwr),
      m_pgood_5v(io.rf_5v_pgood),
      m_pgood_fpga(io.fpga_5v_pgood),
      m_monitor(etl::make_string("current-sensor"), io.mon_i2c, 0b101101),
      m_uhf_en(io.uhf_en),
      m_uhf_pgood(io.uhf_pgood),
      m_sband_en(io.sband_en),
      m_sband_pgood(io.sband_pgood),
      m_imon_5v_rf(io.imon_5v_rf),
      m_imon_3v3(io.imon_3v3_d),
      m_imon_fpga(io.imon_fpga),
      m_v_mon(io.v_mon),
      m_r_lim(io.rlim),
      m_efuse_adc_current_gain(io.efuse_adc_current_gain)

{
  /* Disable all at startup */
  enable(subsys::CAN1, false);
  enable(subsys::CAN2, false);
  enable(subsys::RF_5V, false);
  enable(subsys::FPGA_5V, false);
  enable(subsys::UHF, false);
  enable(subsys::SBAND, false);

  /* Start the ADC of the 3V3 which remains always on */
  m_imon_3v3.start();
}

} // namespace satnogs::comms
